package www.lagou.app;

import org.junit.Test;
import www.lagou.dao.PhoneDao;
import www.lagou.entity.Phone;

import java.sql.SQLException;
import java.util.List;

/**
 * created with IntelliJ IDEA.
 * date: 2020/6/18
 * time: 20:16
 *
 * @author: asiaw
 * Description:
 *      编程题2
 */
public class Test02 {

    PhoneDao phoneDao = new PhoneDao();

    /**
     * 需求一：查询价格高于2000元，生产日期是2019年之前的所有手机
     * @throws SQLException
     */
    @Test
    public void testNeedOne() throws SQLException {

        List<Phone> phones = phoneDao.findNeedOne(2000, "2019");

        for (Phone phone: phones) {
            System.out.println(phone);
        }

    }

    /**
     * 需求2：
     * 测试颜色查询
     * @throws SQLException
     */
    @Test
    public void testFindByColor() throws SQLException {

        List<Phone> phones = phoneDao.findByColor("白色");

        for (Phone phone: phones) {
            System.out.println(phone);
        }

    }
}
