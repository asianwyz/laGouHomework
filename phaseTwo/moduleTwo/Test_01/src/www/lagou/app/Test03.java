package www.lagou.app;

import org.junit.Test;
import www.lagou.dao.EmployeeDao;
import www.lagou.entity.Employee;

import java.sql.SQLException;
import java.util.List;

/**
 * created with IntelliJ IDEA.
 * date: 2020/6/18
 * time: 21:44
 *
 * @author: asiaw
 * Description:
 */
public class Test03 {

    EmployeeDao employeeDao = new EmployeeDao();

    /**
     * 测试需求一
     */
    @Test
    public void testNeedOne() throws SQLException {

        List<Employee> employees = employeeDao.findAllEmployee();

        for (Employee employee : employees) {
            System.out.println(employee);
        }
    }

    /**
     * 测试需求二
     * @throws SQLException
     */
    @Test
    public void testNeedTwo() throws SQLException {

        List<Employee> employees = employeeDao.findAllEmployeeAndDeptName();

        for (Employee employee : employees) {
            StringBuilder sb = new StringBuilder();
            sb.append("姓名：" + employee.getName() + "，薪资：" + employee.getSalary() + "，部门名称：");
            if (null != employee.getDept()) {
                sb.append(employee.getDept().getDeptname());
            } else {
                sb.append("无");
            }
            System.out.println(sb);
        }
    }


}
