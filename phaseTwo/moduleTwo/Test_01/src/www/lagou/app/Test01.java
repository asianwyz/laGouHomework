package www.lagou.app;

import org.apache.commons.dbutils.QueryRunner;
import org.junit.Test;
import www.lagou.dao.AccountDao;
import www.lagou.entity.Account;
import www.lagou.utils.DruidUtils;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * created with IntelliJ IDEA.
 * date: 2020/6/18
 * time: 18:33
 *
 * @author: asiaw
 * Description:
 *      编程题1
 */
public class Test01 {

    AccountDao accountDao = new AccountDao();

    /**
     * 测试转账功能
     * @throws SQLException
     */
    @Test
    public void testTransaction() throws SQLException {
        System.out.println(accountDao.transcation("1122334455", "55443332211", 5000));
    }

    /**
     * 测试卡号查找功能
     * @throws SQLException
     */
    @Test
    public void testFindByCard() throws SQLException {
        Account account = accountDao.findAccountByCard("1122334455");
        System.out.println(account);
    }
}
