package www.lagou.entity;

/**
 * created with IntelliJ IDEA.
 * date: 2020/6/18
 * time: 21:35
 *
 * @author: asiaw
 * Description:
 *      员工类
 */
public class Employee {

    /**
     * 员工id
     */
    private double id;

    /**
     * 员工姓名
     */
    private String name;

    /**
     * 员工年龄
     */
    private double age;

    /**
     * 员工性别
     */
    private String sex;

    /**
     * 薪水
     */
    private double salary;

    /**
     * 入职日期
     */
    private String empdate;

    /**
     * 部门id
     */
    private double deptid;

    /**
     * 所属部门信息
     */
    private Dept dept;

    public double getId() {
        return id;
    }

    public void setId(double id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getAge() {
        return age;
    }

    public void setAge(double age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public String getEmpdate() {
        return empdate;
    }

    public void setEmpdate(String empdate) {
        this.empdate = empdate;
    }

    public double getDeptid() {
        return deptid;
    }

    public void setDeptid(double deptid) {
        this.deptid = deptid;
    }

    public Dept getDept() {
        return dept;
    }

    public void setDept(Dept dept) {
        this.dept = dept;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", sex='" + sex + '\'' +
                ", salary=" + salary +
                ", empdate='" + empdate + '\'' +
                ", deptid=" + deptid +
                '}';
    }
}
