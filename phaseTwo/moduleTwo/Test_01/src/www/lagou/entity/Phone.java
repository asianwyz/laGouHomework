package www.lagou.entity;

/**
 * created with IntelliJ IDEA.
 * date: 2020/6/18
 * time: 20:32
 *
 * @author: asiaw
 * Description:
 *      手机类
 */
public class Phone {

    /**
     * 手机编号
     */
    private int id;

    /**
     * 手机名称
     */
    private String pname;

    /**
     * 价格
     */
    private double price;

    /**
     * 生产日期
     */
    private String prodate;

    /**
     * 颜色
     */
    private String color;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getProdate() {
        return prodate;
    }

    public void setProdate(String prodate) {
        this.prodate = prodate;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Phone{" +
                "id=" + id +
                ", pname='" + pname + '\'' +
                ", price=" + price +
                ", prodate='" + prodate + '\'' +
                ", color='" + color + '\'' +
                '}';
    }
}
