package www.lagou.entity;

/**
 * created with IntelliJ IDEA.
 * date: 2020/6/18
 * time: 21:33
 *
 * @author: asiaw
 * Description:
 *      部门类
 */
public class Dept {

    /**
     * 部门id
     */
    private int id;

    /**
     * 部门名称
     */
    private String deptname;

    public Dept() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDeptname() {
        return deptname;
    }

    public void setDeptname(String deptname) {
        this.deptname = deptname;
    }

    @Override
    public String toString() {
        return "Dept{" +
                "id=" + id +
                ", deptname='" + deptname + '\'' +
                '}';
    }
}
