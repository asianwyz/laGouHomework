package www.lagou.entity;

import java.io.Serializable;

/**
 * created with IntelliJ IDEA.
 * date: 2020/6/18
 * time: 16:16
 *
 * @author: asiaw
 * Description:
 *      交易记录类
 */
public class Transaction implements Serializable {

    /**
     * 交易编号
     */
    private int id;

    /**
     * 交易卡号
     */
    private String cardid;

    /**
     * 交易类型，转入或者转出
     */
    private String tratype;

    /**
     * 交易金额
     */
    private double tramoney;

    /**
     * 交易日期
     */
    private String tradate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCardid() {
        return cardid;
    }

    public void setCardid(String cardid) {
        this.cardid = cardid;
    }

    public String getTratype() {
        return tratype;
    }

    public void setTratype(String tratype) {
        this.tratype = tratype;
    }

    public double getTramoney() {
        return tramoney;
    }

    public void setTramoney(double tramoney) {
        this.tramoney = tramoney;
    }

    public String getTradate() {
        return tradate;
    }

    public void setTradate(String tradate) {
        this.tradate = tradate;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "id=" + id +
                ", cardid='" + cardid + '\'' +
                ", tratype='" + tratype + '\'' +
                ", tramoney=" + tramoney +
                ", tradate='" + tradate + '\'' +
                '}';
    }
}
