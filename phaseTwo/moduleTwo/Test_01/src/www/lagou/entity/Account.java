package www.lagou.entity;

import java.io.Serializable;

/**
 * created with IntelliJ IDEA.
 * date: 2020/6/18
 * time: 14:29
 *
 * @author: asiaw
 * Description:
 *      账号类
 */
public class Account implements Serializable {
    /**
     * 编号
     */
    private int id;

    /**
     * 用户姓名
     */
    private String username;

    /**
     * 卡号
     */
    private String card;

    /**
     * 当前余额
     */
    private double balance;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCard() {
        return card;
    }

    public void setCard(String card) {
        this.card = card;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", card='" + card + '\'' +
                ", balance=" + balance +
                '}';
    }
}
