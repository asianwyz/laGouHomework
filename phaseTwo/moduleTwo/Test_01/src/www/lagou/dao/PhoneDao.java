package www.lagou.dao;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import www.lagou.entity.Phone;
import www.lagou.utils.DruidUtils;

import java.sql.SQLException;
import java.util.List;

/**
 * created with IntelliJ IDEA.
 * date: 2020/6/18
 * time: 20:54
 *
 * @author: asiaw
 * Description:
 *      手机dao类
 */
public class PhoneDao {

    /**
     * 根据颜色查询手机信息
     * @param color 颜色
     * @return 查询到的手机
     */
    public List<Phone> findByColor(String color) throws SQLException {

        QueryRunner qr = new QueryRunner(DruidUtils.getDataSource());

        String sql = "select * from phone where color = ?";

        List<Phone> phones = qr.query(sql, new BeanListHandler<>(Phone.class), color);

        return phones;
    }

    /**
     * 需求1：查询价格高于 xxx ，生产日期是 xxx 之前的所有手机
     * @param price     手机价格
     * @param date      生产日期
     * @return
     */
    public List<Phone> findNeedOne(double price, String date) throws SQLException {

        QueryRunner qr = new QueryRunner(DruidUtils.getDataSource());

        String sql = "select * from phone where price > ? and prodate < ?";

        Object[] params = {price, date};

        List<Phone> phones = qr.query(sql, new BeanListHandler<>(Phone.class), params);

        return phones;
    }
}
