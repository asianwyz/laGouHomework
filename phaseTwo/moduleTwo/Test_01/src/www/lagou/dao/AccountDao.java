package www.lagou.dao;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import www.lagou.entity.Account;
import www.lagou.utils.DateUtils;
import www.lagou.utils.DruidUtils;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * created with IntelliJ IDEA.
 * date: 2020/6/18
 * time: 18:41
 *
 * @author: asiaw
 * Description:
 */
public class AccountDao {

    /**
     * 通过id查找账号
     * @param id  要查找账号的id
     * @return
     * @throws SQLException
     */
    public Account findAccountById(int id) throws SQLException {
        QueryRunner qr = new QueryRunner(DruidUtils.getDataSource());

        String sql = "select * from account where id = ?";

        Account account = qr.query(sql, new BeanHandler<>(Account.class), id);

        return account;
    }

    /**
     * 通过卡号查找账号
     * @param card
     * @return
     * @throws SQLException
     */
    public Account findAccountByCard(String card) throws SQLException {
        QueryRunner qr = new QueryRunner(DruidUtils.getDataSource());

        String sql = "select * from account where card = ?";

        Account account = qr.query(sql, new BeanHandler<>(Account.class), card);

        return account;
    }

    /**
     * 转账操作
     * @param outCard 转出卡号
     * @param inCard  转入卡号
     * @param traMoney  交易金额
     * @return
     */
    public String transcation(String outCard, String inCard, double traMoney) throws SQLException {

        // 获取转出账号信息
        Account accountOut = null;
        accountOut = findAccountByCard(outCard);
        if (accountOut == null) {
            return "没有转出账户的信息";
        }

        // 获取转入账号信息
        Account accountIn = null;
        accountIn = findAccountByCard(inCard);
        if (accountIn == null) {
            return "没有转入账户的信息";
        }

        // 判断余额是否充足
        if (compareto(accountOut.getBalance(), traMoney)) {
            return "余额不足";
        }

        // 获取连接
        QueryRunner qr = new QueryRunner();
        Connection con = DruidUtils.getConnection();

        try {
            // 开启事务
            con.setAutoCommit(false);

            // sql语句
            String sqlOut = "update account set balance = balance - ? where card = ?";
            String sqlIn = "update account set balance = balance + ? where card = ?";

            // 转出操作
            Object[] outParam = {traMoney, outCard};
            qr.update(con, sqlOut, outParam);

            // 转入操作
            Object[] inParam = {traMoney, inCard};
            qr.update(con, sqlIn, inParam);

            // 获取时间
            String date = DateUtils.getDateFormart();
            // sql语句
            String insert = "insert into TRANSACTION (cardid, tratype, tramoney, tradate) values(?, ?, ?, ?)";

            Object[] insertOUt = {outCard, "转出", traMoney, date};
            Object[] insertIn = {inCard, "转入", traMoney, date};
            // 记录转账操作
            qr.update(con, insert, insertIn);
            qr.update(con, insert, insertOUt);

            // 提交事务
            con.commit();
            return "转账成功";

        } catch (SQLException throwables) {
            // 回滚
            con.rollback();
            throwables.printStackTrace();
            return "转账失败";
        }

    }

    /**
     * 检查账号余额是否充足
     * @param balance   账号余额
     * @param traMoney  转账金额
     * @return  余额充足返回 true
     */
    private boolean compareto(double balance, double traMoney) {
        BigDecimal bigDecimal1 = new BigDecimal(balance);
        BigDecimal bigDecimal2 = new BigDecimal(traMoney);
        if (bigDecimal2.compareTo(bigDecimal1) > 0) {
            return true;
        }
        return false;
    }
}
