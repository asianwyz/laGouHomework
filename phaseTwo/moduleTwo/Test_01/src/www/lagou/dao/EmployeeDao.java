package www.lagou.dao;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.junit.Test;
import www.lagou.entity.Dept;
import www.lagou.entity.Employee;
import www.lagou.utils.DruidUtils;

import java.sql.SQLException;
import java.util.List;

/**
 * created with IntelliJ IDEA.
 * date: 2020/6/18
 * time: 21:39
 *
 * @author: asiaw
 * Description:
 */
public class EmployeeDao {

    /**
     * 需求一：
     * 查询所有员工信息（不包含没有部门的员工）
     * @return
     */
    public List<Employee> findAllEmployee() throws SQLException {
        QueryRunner qr = new QueryRunner(DruidUtils.getDataSource());

        String sql = "select * from employee where deptid is not null";

        List<Employee> employees = qr.query(sql, new BeanListHandler<>(Employee.class));

        return employees;
    }

    /**
     * 查询所有员工信息（包括没有部门的员工）
     * @return
     * @throws SQLException
     */
    public List<Employee> findAllEmployeeHasNullDept() throws SQLException {
        QueryRunner qr = new QueryRunner(DruidUtils.getDataSource());

        String sql = "select * from employee";

        List<Employee> employees = qr.query(sql, new BeanListHandler<>(Employee.class));

        return employees;
    }

    /**
     * 根据部门id查询部门信息
     * @param deptid 部门id
     * @return
     */
    public Dept findDeptById(int deptid) throws SQLException {

        QueryRunner qr = new QueryRunner(DruidUtils.getDataSource());

        String sql = "select * from dept where id = ?";

        Dept dept = qr.query(sql, new BeanHandler<>(Dept.class), deptid);

        return dept;
    }

    /**
     * 需求二：
     * 查询每个员工的姓名，薪资和所属部门
     * @return
     */
    @Test
    public List<Employee> findAllEmployeeAndDeptName() throws SQLException {
        List<Employee> employees = findAllEmployeeHasNullDept();

        for (Employee employee : employees) {
            Dept dept = findDeptById((int) employee.getDeptid());
            employee.setDept(dept);
        }

        return employees;
    }
}
