-- 第一题
-- 1. 查询平均成绩大于70分的同学的学号，姓名和平均成绩`course`

SELECT 
	s.`id`,
	s.`NAME`,
	st.avgscore
FROM student s
INNER JOIN (SELECT s.`student_id` id,
	AVG(s.`score`) avgscore
	FROM student_course AS s
	GROUP BY s.`student_id`
	HAVING AVG(s.score) > 70) st
ON s.id = st.id;

-- 第二题
-- 2. 查询所有同学的学号、姓名、选课数、总成绩
SELECT 
	s.`id`,
	s.`NAME`,
	sc.courses '选课数',
	sc.scores '总成绩'
FROM student s
JOIN (SELECT sc.`student_id`, COUNT(*) courses, SUM(score) scores
FROM student_course sc
GROUP BY sc.`student_id`) sc
ON s.`id` = sc.`student_id`;

-- 第三题
-- 3. 查询学过赵云老师课程的同学的学号、姓名
SELECT s.`id`,
       s.`NAME`	
FROM student s
JOIN student_course sc
ON s.`id` = sc.`student_id`
WHERE 
sc.`course_id` = (SELECT c.`id` FROM course c
		  JOIN teacher t
		  ON c.`teacher_id` = t.`id`
		  WHERE t.`NAME` = '赵云');


-- 第四题
-- 4. 查询选课少于3门学科的学员
SELECT 
	s.`id`,
	s.`NAME`
FROM student s
WHERE s.`id` IN (SELECT sc.`student_id`
		FROM student_course sc
		GROUP BY sc.`student_id`
		HAVING COUNT(*) < 3);






















	
	