-- 创建讲师表
CREATE TABLE lagou_teacher (
	id INT PRIMARY KEY, -- 讲师id
	NAME VARCHAR(50), -- 讲师姓名
	introduction VARCHAR(200), -- 讲师简介
	LEVEL CHAR(4) -- 讲师级别（高级讲师&首席讲师）
);
-- 为讲师姓名添加普通索引
ALTER TABLE lagou_teacher ADD INDEX teacherName(NAME);

-- 创建课程分类表
CREATE TABLE lagou_subject (
	id INT PRIMARY KEY, -- 课程分类id
	NAME VARCHAR(20), -- 课程分类名称
	description VARCHAR(200), -- 课程分类描述
	createDate DATETIME, -- 创建时间
	updateDate DATETIME -- 更新时间
);

-- 创建课程表
CREATE TABLE lagou_course(
	id INT PRIMARY KEY, -- 课程id
	teacher_id INT, -- 课程讲师id，外键
	subject_id INT, -- 课程分类id，外键
	title VARCHAR(20), -- 课程标题
	classHour INT, -- 总课时
	countView BIGINT, -- 浏览数量
	state CHAR(1) DEFAULT 0, -- 课程状态，0 未发布（默认）1（已发布）
	-- 添加外键约束
	CONSTRAINT teacher_id_fk FOREIGN KEY(teacher_id) REFERENCES lagou_teacher(id),
	CONSTRAINT subject_id_fk FOREIGN KEY(subject_id) REFERENCES lagou_subject(id)
);

-- 为课程标题字段添加普通索引
ALTER TABLE lagou_course ADD INDEX tileRe(title);


-- 向讲师表插入两条数据 
INSERT INTO lagou_teacher VALU`lagou_course`ES (1, '刘德华', '毕业于清华大学，主攻前端技术,授课风格生动活泼,深受学员喜爱', '高级讲师'); 
INSERT INTO lagou_teacher VALUES (2, '郭富城', '毕业于北京大学，多年的IT经验，研发多项Java课题,授课经验丰富', '首席讲师');

-- 向课程分类表中插入两条数据 
INSERT INTO lagou_subject VALUES (1, '后端开发', '后端课程包括 Java PHP Python', '2020-03-27 00:44:04', '2020-03-27 00:44:04'); 
INSERT INTO lagou_subject VALUES (2, '前端开发', '前端课程包括 JQuery VUE angularJS', '2020-02-27 10:00:04', '2020-02-27 18:44:04');

-- 向课程表中插入两条数据 
-- 插入Java课程 
INSERT INTO lagou_course VALUES (1,1,2 ,'Java', 300,250000, '1'); 
-- 插入VUE课程 
INSERT INTO lagou_course VALUES (2,2,1, 'VUE', 400,200000,'1');

-- 查询刘德华老师所教的课程属于哪个课程分类

SELECT ls.`name` '课程分类'
FROM lagou_subject ls
WHERE ls.`id` = (
	SELECT lc.`subject_id`
	FROM lagou_course lc
	WHERE lc.`teacher_id` = (
		SELECT lt.`id`
		FROM lagou_teacher lt
		WHERE lt.`name` = '刘德华'
	)
);

















