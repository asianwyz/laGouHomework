## 任务一
![输入图片说明](https://images.gitee.com/uploads/images/2020/0613/082003_11518466_5579290.png "屏幕截图.png")

解析：查询速度比普通文件快。

## 任务二
![输入图片说明](https://images.gitee.com/uploads/images/2020/0613/083044_69ddaad7_5579290.png "屏幕截图.png")
解析：应该是点错了吧

## 任务三
![输入图片说明](https://images.gitee.com/uploads/images/2020/0613/083141_5b0c7d6a_5579290.png "屏幕截图.png")
解析：我以为外键值不能为空

## 任务四
![输入图片说明](https://images.gitee.com/uploads/images/2020/0613/083256_0bfaec3a_5579290.png "屏幕截图.png")
解析：数据量过大，对某些经常查询的字段添加索引不是可以加快查询速度吗？不是很理解

![输入图片说明](https://images.gitee.com/uploads/images/2020/0613/083343_4da8e2f9_5579290.png "屏幕截图.png")
解析：看出要选正确的啦，完美错过答案
