# 1. 查询工资最高的员工是谁
SELECT * FROM employee WHERE salary = (SELECT MAX(salary) FROM employee);

# 2. 查询工资小于平均工资的员工有哪些
SELECT * FROM employee WHERE salary < (SELECT AVG(salary) FROM employee);

# 3. 查询大于5000的员工，来自于哪些部门，输出部门的名字
SELECT DISTINCT d.`name` '部门'
FROM employee AS e JOIN dept AS d
ON e.`dept_id` = d.`id`
WHERE salary > 5000;

# 4. 查询开发部与财务部所有的员工信息，分别使用子查询和表连接实现
-- 表连接
SELECT e.*
FROM employee e 
JOIN dept d
ON e.`dept_id` = d.`id`
WHERE d.`name` IN ('开发部', '财务部');

-- 子查询
SELECT * FROM employee
WHERE dept_id IN (SELECT id FROM dept WHERE NAME IN ('开发部', '财务部'));

# 5. 查询2011年以后入职的员工信息和部门信息
-- 表连接
SELECT e.*, d.name '部门'
FROM employee e
JOIN dept d
ON e.`dept_id` = d.`id`
WHERE e.`join_date` > '2012';

-- 子查询
SELECT e.*, d.`name` '部门'
FROM dept d
JOIN (
	SELECT * FROM employee e
	WHERE e.`join_date` > '2012'
) e
ON e.dept_id = d.id











