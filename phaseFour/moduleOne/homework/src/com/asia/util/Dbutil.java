package com.asia.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * created with IntelliJ IDEA.
 * date: 2020/10/24
 * time: 11:14
 *
 * @author: asiaw
 * Description:
 */
public class Dbutil {

    private static String jdbcName;
    private static String dburl;
    private static String dbusername;
    private static String dbpassword;


//  成员静态初始化
    static {
        jdbcName = "com.mysql.jdbc.Driver";
        dburl = "jdbc:mysql://localhost:3306/db_web";
        dbusername = "root";
        dbpassword = "root";

        try {
            Class.forName(jdbcName);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static Connection getConnection() throws SQLException {

        Connection con = DriverManager.getConnection(dburl, dbusername, dbpassword);
        return con;
    }

    public static void closeConnection(Connection con, PreparedStatement pst) throws SQLException {
        if (null != con) {
            con.close();
        }

        if (null != pst) {
            pst.close();
        }
    }

}
