package com.asia.servlet;

import com.asia.service.StudentService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * created with IntelliJ IDEA.
 * date: 2020/10/27
 * time: 15:21
 *
 * @author: asiaw
 * Description:
 */
@WebServlet(name = "DeleteServlet", urlPatterns = "/delete")
public class DeleteServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//      获取要删除学生id
        String id = request.getParameter("id");
        System.out.println(id);

//        获取Service层
        StudentService studentService = new StudentService();

//        删除
        int x = studentService.delete(id);

        response.sendRedirect("main.jsp");

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
