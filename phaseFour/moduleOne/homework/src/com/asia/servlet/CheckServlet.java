package com.asia.servlet;

import com.asia.bean.Student;
import com.asia.service.StudentService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * created with IntelliJ IDEA.
 * date: 2020/10/27
 * time: 11:57
 *
 * @author: asiaw
 * Description:
 */
@WebServlet(name = "CheckServlet", urlPatterns = "/check")
public class CheckServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//      设置编码
        request.setCharacterEncoding("utf-8");

//        获取查询数据
        String check = request.getParameter("check");
        System.out.println(check);

//        获取Service
        StudentService studentService = new StudentService();
//      准备接收容器
        List<Student> checkStudents = new ArrayList<>();
//      根据名字查询
        Student student = studentService.findById(check);
        System.out.println("servlet: " + student);
        if (student != null) {
            checkStudents.add(student);
        }
//      根据姓名查询
        List<Student> list = studentService.findByName(check);

        if (list.size() > 0) {
            checkStudents.addAll(list);
        }
//        将查询结果返回给查询页面
        request.getSession().setAttribute("checkStudents", checkStudents);
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("checkView.jsp");
        requestDispatcher.forward(request, response);

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
