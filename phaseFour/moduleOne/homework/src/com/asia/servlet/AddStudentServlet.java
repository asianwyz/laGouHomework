package com.asia.servlet;

import com.asia.bean.Student;
import com.asia.service.StudentService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * created with IntelliJ IDEA.
 * date: 2020/10/26
 * time: 19:45
 *
 * @author: asiaw
 * Description:
 */
@WebServlet(name = "AddStudentServlet", urlPatterns = "/addStudent")
public class AddStudentServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

//        设置编码
        request.setCharacterEncoding("utf-8");

//        获取表单提交的学生数据
        String id = request.getParameter("id");
        String name = request.getParameter("name");
        String sex = request.getParameter("sex");
        String birthday = request.getParameter("birthday");
        String mail = request.getParameter("mail");
        String remark = request.getParameter("remark");

        Student student = new Student(id, name, sex, birthday, mail, remark);
        System.out.println("正在添加学生信息");
        System.out.println(student);

//      获取Service，进行添加操作
        StudentService studentService = new StudentService();
        int i = studentService.addStudent(student);

//        在session中设置添加信息
        if (1 == i) {
//            添加成功
            request.setAttribute("add", "学生信息添加成功");
//            RequestDispatcher requestDispatcher = request.getRequestDispatcher("main.jsp");
//            requestDispatcher.forward(request, response);
        } else {
            request.setAttribute("add", "添加失败，请重新提交信息");
            System.out.println("学生信息添加失败");
        }
//        回到添加页面
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("addStudent.jsp");
        requestDispatcher.forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
