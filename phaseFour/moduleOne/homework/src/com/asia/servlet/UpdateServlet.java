package com.asia.servlet;

import com.asia.bean.Student;
import com.asia.service.StudentService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * created with IntelliJ IDEA.
 * date: 2020/10/26
 * time: 19:45
 *
 * @author: asiaw
 * Description:
 */
@WebServlet(name = "UpdateServlet", urlPatterns = "/update")
public class UpdateServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//      设置编码
        request.setCharacterEncoding("utf-8");
//      获取更新的学生数据
        String id = request.getParameter("id");
        String name = request.getParameter("name");
        String sex = request.getParameter("sex");
        String birthday = request.getParameter("birthday");
        String mail = request.getParameter("mail");
        String remark = request.getParameter("remark");

        Student student = new Student(id, name, sex, birthday, mail, remark);
        System.out.println("正在修改学生信息");
        System.out.println(student);

//      获取Service层，进行修改
        StudentService studentService = new StudentService();
        int i = studentService.update(student);
        request.getSession().setAttribute("update", i);

        if (1 == i) {
//            修改成功返回至首页
           response.sendRedirect("main.jsp");
        } else {
//            修改失败返回修改页面
            RequestDispatcher requestDispatcher = request.getRequestDispatcher("update.jsp?id=" + id);
            requestDispatcher.forward(request, response);
            System.out.println("学生信息修改失败");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
