package com.asia.servlet;

import com.asia.bean.Student;
import com.asia.bean.User;
import com.asia.service.StudentService;
import com.asia.service.UserService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * created with IntelliJ IDEA.
 * date: 2020/10/26
 * time: 11:13
 *
 * @author: asiaw
 * Description:
 *      登录servlet
 */
@WebServlet(name = "LoginServlet", urlPatterns = "/loginIn")
public class LoginServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

//       获取登录信息
        String username = request.getParameter("userName");
        System.out.println("获取到的用户名为：" + username);

        String password = request.getParameter("password");
        System.out.println("获取到的密码为：" + password);

        // 进行检验
        UserService userService = new UserService();
        User user = userService.userLoginService(new User(username, password));

//        返回前台
        if (null == user) {
            System.out.println("登录失败");
            request.setAttribute("error", "登录失败，用户名或密码错误！");
            // 实现服务器跳转  共享request和response对象
            RequestDispatcher requestDispatcher = request.getRequestDispatcher("index.jsp");
            requestDispatcher.forward(request, response);
        } else {
            System.out.println("登录成功");
            request.getSession().setAttribute("user", user);
            // 跳转至主页面
            response.sendRedirect("main.jsp");
        }

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);

    }
}
