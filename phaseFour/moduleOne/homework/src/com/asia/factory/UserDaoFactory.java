package com.asia.factory;

import com.asia.dao.UserDao;
import com.asia.dao.UserDaoImp;

/**
 * created with IntelliJ IDEA.
 * date: 2020/10/26
 * time: 15:08
 *
 * @author: asiaw
 * Description:
 * 工厂类，返回UserDao
 */
public class UserDaoFactory {

    public static UserDao getUserDao() {
        return new UserDaoImp();
    }
}
