package com.asia.factory;

import com.asia.dao.StudentDao;
import com.asia.dao.StudentDaoImp;
import com.asia.dao.UserDao;
import com.asia.dao.UserDaoImp;

/**
 * created with IntelliJ IDEA.
 * date: 2020/10/26
 * time: 18:29
 *
 * @author: asiaw
 * Description:
 *      工厂类，返回StudentDao
 */
public class StudentDaoFactory {

    public static StudentDao getStudentDao() {
        return new StudentDaoImp();
    }
}
