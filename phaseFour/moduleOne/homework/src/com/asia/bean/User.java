package com.asia.bean;

import java.io.Serializable;
import java.util.Objects;

/**
 * created with IntelliJ IDEA.
 * date: 2020/10/24
 * time: 10:48
 *
 * @author: asiaw
 * Description:
 *      管理员类
 */
public class User implements Serializable {

    private String username;
    private String password;

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "User{" +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

}
