package com.asia.bean;

import java.io.Serializable;
import java.util.Date;

/**
 * created with IntelliJ IDEA.
 * date: 2020/10/26
 * time: 15:21
 *
 * @author: asiaw
 * Description:
 *
 *      学生类
 */
public class Student implements Serializable {

    private String id;
    private String name;
    private String sex;
    private String birthday;
    private String mail;
    private String remark;

    public Student() {
    }

    public Student(String id, String name, String sex, String birthday, String mail, String remark) {
        this.id = id;
        this.name = name;
        this.sex = sex;
        this.birthday = birthday;
        this.mail = mail;
        this.remark = remark;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", sex='" + sex + '\'' +
                ", birthday=" + birthday +
                ", mail='" + mail + '\'' +
                ", remark='" + remark + '\'' +
                '}';
    }
}
