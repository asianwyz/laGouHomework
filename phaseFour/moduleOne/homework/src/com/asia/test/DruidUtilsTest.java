package com.asia.test;

import com.asia.util.DruidUtils;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * created with IntelliJ IDEA.
 * date: 2020/10/26
 * time: 23:17
 *
 * @author: asiaw
 * Description:
 */
public class DruidUtilsTest {

    public static void main(String[] args) throws SQLException {

        Connection con = DruidUtils.getConnection();

        Statement statement = con.createStatement();

        ResultSet resultSet = statement.executeQuery("select * from t_user");

        while (resultSet.next()) {
            System.out.println("用户名：" + resultSet.getString(1) + "，密码：" + resultSet.getString(2));
        }

        DruidUtils.close(con, statement, resultSet);

    }
}
