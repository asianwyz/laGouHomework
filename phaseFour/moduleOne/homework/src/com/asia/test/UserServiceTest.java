package com.asia.test;

import com.asia.bean.User;
import com.asia.dao.UserDao;
import com.asia.dao.UserDaoImp;
import com.asia.service.UserService;

/**
 * created with IntelliJ IDEA.
 * date: 2020/10/26
 * time: 11:59
 *
 * @author: asiaw
 * Description:
 */
public class UserServiceTest {

    public static void main(String[] args) {

        UserService userService = new UserService();

        User admin = userService.userLoginService(new User("admin", "123456"));
        System.out.println(admin);
    }
}
