package com.asia.test;

import com.asia.bean.Student;
import com.asia.service.StudentService;
import org.junit.Test;

import java.util.List;

/**
 * created with IntelliJ IDEA.
 * date: 2020/10/26
 * time: 18:48
 *
 * @author: asiaw
 * Description:
 *     测试学生service类
 */
public class StudentServiceTest {



    public static void main(String[] args) {
        StudentService studentService = new StudentService();
        List<Student> list = studentService.findAll();
        for (Student stu : list) {
            System.out.println(stu);
        }
    }

    @Test
    public void testAdd() {
        StudentService studentService = new StudentService();

        for (int i = 100; i < 400; i++) {
            Student stu = new Student("M2020" + i, "bob" + i, + i % 2 == 0 ? "男" : "女",
                    "1999-0-" + i, "2121" + i + "@qq.com", "asdgjairog" + i);
            studentService.addStudent(stu);

        }
    }

    @Test
    public void testFindById() {
        StudentService studentService = new StudentService();
        Student student = studentService.findById("121");
        System.out.println(student);
    }

    @Test
    public void testFindByName() {
        StudentService studentService = new StudentService();
        List<Student> students = studentService.findByName("Bobb");
        for (Student stu : students) {
            System.out.println(stu);
        }
        if (students.size() == 0) {
            System.out.println("没有此人信息");
        }
    }

    @Test
    public void testUpdate() {
        StudentService studentService = new StudentService();

        Student student = new Student("14", "zsf", "女", "1978-1-1", "2fe38298@1sd63.com", "asdfasgjalksdasdfasdfjfasldjf");

        System.out.println(studentService.update(student));

    }

    @Test
    public void testDelete() {
        StudentService studentService = new StudentService();
        System.out.println(studentService.delete("11"));
    }

    @Test
    public void testPage() {
        StudentService studentService = new StudentService();

        List<Student> list = studentService.getByPage(3, 3);

        for (Student stu : list) {
            System.out.println(stu);
        }
    }

    @Test
    public void testCount() {
        StudentService studentService = new StudentService();
        System.out.println(studentService.count());
    }
}
