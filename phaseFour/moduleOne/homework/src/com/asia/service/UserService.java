package com.asia.service;

import com.asia.bean.User;
import com.asia.dao.UserDao;
import com.asia.factory.UserDaoFactory;

/**
 * created with IntelliJ IDEA.
 * date: 2020/10/26
 * time: 11:58
 *
 * @author: asiaw
 * Description:
 *      Service 层，较少耦合
 */
public class UserService {

    private UserDao userDao;

    public UserService() {
        this.userDao = UserDaoFactory.getUserDao();
    }

    public User userLoginService(User user) {
        return userDao.userLogin(user);
    }
}
