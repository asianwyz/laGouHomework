package com.asia.service;

import com.asia.bean.Student;
import com.asia.dao.StudentDao;
import com.asia.factory.StudentDaoFactory;

import java.util.List;

/**
 * created with IntelliJ IDEA.
 * date: 2020/10/26
 * time: 18:29
 *
 * @author: asiaw
 * Description:
 *      Service层，减少耦合
 */
public class StudentService {

    private StudentDao studentDao;

    public StudentService() {
        this.studentDao = StudentDaoFactory.getStudentDao();
    }

    /**
     * 寻找所有学生信息
     * @return
     */
    public List<Student> findAll() {
        return studentDao.findAll();
    }

    /**
     * 根据id查找学生信息
     * @param id
     * @return
     */
    public Student findById(String id) {
        return studentDao.findById(id);
    }

    /**
     * 根据name查找学生信息
     * @param name
     * @return
     */
    public List<Student> findByName(String name) {
        return studentDao.findByName(name);
    }

    /**
     * 添加学生信息
     * @param student
     * @return
     */
    public int addStudent(Student student) {
        return studentDao.addStudent(student);
    }


    /**
     * 更改学生信息
     * @param student
     * @return
     */
    public int update(Student student) {
        return studentDao.update(student);
    }

    /**
     * 删除指定学号学生
     * @param id
     * @return
     */
    public int delete(String id) {
        return studentDao.delete(id);
    }

    /**
     * 获取总人数
     * @return
     */
    public int count() {
        return studentDao.count();
    }

    /**
     * 获取总页数
     * @param pageSize
     * @return
     */
    public int totalPage(int pageSize) {
        return studentDao.totalPage(pageSize);
    }

    /**
     * 根据页数获取数据
     * @param pageSize
     * @param pageIndex
     * @return
     */
    public List<Student> getByPage(int pageSize, int pageIndex) {
        return studentDao.getByPage(pageSize, pageIndex);
    }
}
