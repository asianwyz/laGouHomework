package com.asia.dao;

import com.asia.bean.Student;

import java.util.List;

/**
 * created with IntelliJ IDEA.
 * date: 2020/10/26
 * time: 17:59
 *
 * @author: asiaw
 * Description:
 *      学生Dao接口
 */
public interface StudentDao {

    /**
     * 获取所有学生信息
     * @return
     */
    public abstract List<Student> findAll();

    /**
     * 根据id查找信息
     * @param id
     * @return
     */
    public abstract Student findById(String id);

    /**
     * 根据名字查找信息
     * @param name
     * @return
     */
    public abstract List<Student> findByName(String name);

    /**
     * 添加学生信息
     * @param student
     * @return
     */
    public abstract int addStudent(Student student);

    /**
     * 修改学生信息
     * @param student
     * @return
     */
    public abstract int update(Student student);

    /**
     * 删除学生信息
     * @param id
     * @return
     */
    public abstract int delete(String id);

    /**
     * 获取总学生人数
     * @return
     */
    public abstract int count();

    /**
     * 总显示页数
     * @param pageSize
     * @return
     */
    public abstract int totalPage(int pageSize);

    /**
     * 根据页数查询数据
     * @param pageSize
     * @param pageIndex
     * @return
     */
    public abstract List<Student> getByPage(int pageSize, int pageIndex);
}
