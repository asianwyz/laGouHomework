package com.asia.dao;

import com.asia.bean.User;
import com.asia.util.Dbutil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


/**
 * created with IntelliJ IDEA.
 * date: 2020/10/26
 * time: 11:28
 *
 * @author: asiaw
 * Description:
 */
public class UserDaoImp implements UserDao{


    /**
     * 不用连接池查询
     * @param user
     * @return
     */
    @Override
    public User userLogin(User user) {
        // 1. 获取数据库链接
        // 2. 准备sql语句

        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;


        try {
            connection = Dbutil.getConnection();
            String sql = "select * from t_user where username = ? and password = ?";

            // 3. 执行语句
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, user.getUsername());
            preparedStatement.setString(2, user.getPassword());

            // 4. 获取结果
            resultSet = preparedStatement.executeQuery();


            if (resultSet.next()) {
                User tu = new User(resultSet.getString("username"), resultSet.getString("password"));
                return tu;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // 5. 释放资源
            try {
                Dbutil.closeConnection(connection, preparedStatement);
                if (null != resultSet) {
                    resultSet.close();
                }
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        System.out.println("查找失败");
        // 查找失败
        return null;
    }
}
