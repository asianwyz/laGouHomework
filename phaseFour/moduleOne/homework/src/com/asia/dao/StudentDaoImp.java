package com.asia.dao;

import com.asia.bean.Student;
import com.asia.util.Dbutil;
import com.asia.util.DruidUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * created with IntelliJ IDEA.
 * date: 2020/10/26
 * time: 18:06
 *
 * @author: asiaw
 * Description:
 *      具体查询逻辑实现
 */
public class StudentDaoImp implements StudentDao{

    /**
     * 查询所有学生信息
     *      不用连接池查询
     * @return
     */
    @Override
    public List<Student> findAll() {
        // 1. 获取数据库链接
        // 2. 准备sql语句

        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;


        try {
            connection = Dbutil.getConnection();
            String sql = "select * from student";

            // 3. 执行语句
            preparedStatement = connection.prepareStatement(sql);

            // 4. 获取结果
            resultSet = preparedStatement.executeQuery();

            List<Student> list = new ArrayList<>();

            while (resultSet.next()) {
                String id = resultSet.getString("id");
                String name = resultSet.getString("name");
                String sex = resultSet.getString("sex");
                String birthday = resultSet.getString("birthday");
                String mail = resultSet.getString("mail");
                String remark = resultSet.getString("remark");
                list.add(new Student(id, name, sex, birthday, mail, remark));
            }
            return list;

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // 5. 释放资源
            try {
                Dbutil.closeConnection(connection, preparedStatement);
                if (null != resultSet) {
                    resultSet.close();
                }
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        System.out.println("查找失败");
        // 查找失败
        return null;
    }


    /**
     * 根据id查询学生信息
     * @param id
     * @return
     */
    @Override
    public Student findById(String id) {

        // 获取qr
        QueryRunner qr = new QueryRunner(DruidUtils.getDataSource());

        // SQL语句
        String sql = "select * from student where id = ?";

        Student student = null;
        try {
            // 获取结果
            student = qr.query(sql, new BeanHandler<>(Student.class), id);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return student;
    }

    /**
     * 根据名字查询学生信息
     * @param name
     * @return
     */
    @Override
    public List<Student> findByName(String name) {
        QueryRunner qr = new QueryRunner(DruidUtils.getDataSource());

        String sql = "select * from student where name = ?";
        List<Student> students = null;

        try {
            students = qr.query(sql, new BeanListHandler<>(Student.class), name);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return students;
    }


    /**
     * 根据页数查询
     * @param pageSize
     * @param pageIndex
     * @return
     */
    @Override
    public List<Student> getByPage(int pageSize, int pageIndex) {
        QueryRunner qr = new QueryRunner(DruidUtils.getDataSource());

        String sql = "select * from student limit ?, ?";

        Object[] params = {pageSize * (pageIndex - 1), pageSize};

        try {
            List<Student> students = qr.query(sql, new BeanListHandler<>(Student.class), params);
            return students;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return null;
    }

    /**
     * 添加学生信息
     * @param student
     * @return
     */
    @Override
    public int addStudent(Student student) {

        QueryRunner queryRunner = new QueryRunner();

        String sql = "insert into student values(?,?,?,?,?,?)";

        Object[] param = {student.getId(), student.getName(), student.getSex(), student.getBirthday(), student.getMail(), student.getRemark()};

        Connection con = DruidUtils.getConnection();

        try {
            int i = queryRunner.update(con, sql, param);
            return i;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }  finally {
            try {
                con.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }

        return 0;
    }

    /**
     * 修改学生信息
     * @param student
     * @return
     */
    @Override
    public int update(Student student) {
        QueryRunner qr = new QueryRunner(DruidUtils.getDataSource());

        String sql = "update student set name = ?, sex = ?, birthday = ?, mail = ?, remark = ? where id = ?";

        Object[] param = {student.getName(), student.getSex(), student.getBirthday(), student.getMail(), student.getRemark(), student.getId()};

        int x = -1;

        try {
            x = qr.update(sql, param);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return x;
    }

    /**
     * 删除指定id学生信息
     * @param id
     * @return
     */
    @Override
    public int delete(String id) {
        QueryRunner qr = new QueryRunner(DruidUtils.getDataSource());

        String sql = "delete from student where id = ?";

        int x = -1;
        try {
            x = qr.update(sql, id);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return x;
    }

    /**
     * 查找学生总人数
     * @return
     */
    @Override
    public int count() {
        QueryRunner qr = new QueryRunner(DruidUtils.getDataSource());

        String sql = "SELECT COUNT(*) FROM student";

        try {
            int x = ((Long) qr.query(sql, new ScalarHandler())).intValue();
            return x;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return 0;
    }

    /**
     * 显示总页数
     * @param pageSize
     * @return
     */
    @Override
    public int totalPage(int pageSize) {
        int count = count();
        return (count % pageSize == 0 ? count / pageSize : count / pageSize + 1);
    }



}
