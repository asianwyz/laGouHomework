package com.asia.dao;

import com.asia.bean.User;

/**
 * created with IntelliJ IDEA.
 * date: 2020/10/26
 * time: 11:27
 *
 * @author: asiaw
 * Description:
 */
public interface UserDao {

    /**
     * 查找管理员信息
     * @param user
     * @return
     */
    public abstract User userLogin(User user);
}
