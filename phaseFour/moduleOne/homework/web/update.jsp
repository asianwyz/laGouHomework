<%@ page import="com.asia.bean.User" %>
<%@ page import="com.asia.bean.Student" %>
<%@ page import="com.asia.service.StudentService" %><%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2020/10/26
  Time: 19:33
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>添加学生信息</title>
    <link rel="stylesheet" href="resources/css/bootstrap.min.css">
</head>

<style>

    #content {
        width: 1000px;
        margin-right: auto;
        margin-left: auto;
        margin-top: 30px;
    }

</style>

<body>

    <%
        if (session.getAttribute("user") ==null){
            out.print("<script language='JavaScript'>alert('请先登录！');window.location.href='login.jsp'</script>");
        }
    %>



    <%
        // 获取要更改信息的学生信息
        String id = request.getParameter("id");
        StudentService studentService = new StudentService();
        Student student = studentService.findById(id);
    %>

    <div id="content">

        <div>
            <h2>欢迎使用学生管理系统，<%= ((User) session.getAttribute("user")).getUsername()%></h2>
        </div>

<%--        更改表单，将学生信息显示在其中--%>
        <form action="update" method="post">

            <input style="display: none" name="id" value="<%=student.getId()%>">

            <div class="form-group">
                <label>姓名</label>
                <input type="text" class="form-control" name="name" value="<%=student.getName()%>">
            </div>
            <div class="form-group">
                <span>性别</span>
                <input type="radio" name="sex" value="男" checked="checked">男
                <input type="radio" name="sex" value="女">女
            </div>
            <div class="form-group">
                <label>出生日期</label>
                <input type="passowd" class="form-control" value="<%=student.getBirthday()%>" name="birthday">
            </div>

            <div class="form-group">
                <label>邮箱</label>
                <input type="email" class="form-control" value="<%=student.getMail()%>" name="mail">
            </div>

            <div class="form-group">
                <label for="comment">备注:</label>
                <textarea class="form-control" rows="5" id="comment" name="remark"><%=student.getRemark()%></textarea>
            </div>
            <div class="form-group">
                <button class="btn btn-primary">提交</button>
            </div>
        </form>

        <%
            if (request.getAttribute("update") != null) {
                int x = (Integer) request.getAttribute("update");
                if (x == 0) {
//
        %>
<%----%>
            <span style="color: red"><%= " 修改失败，请重新修改信息" %></span>
<%----%>
        <% }} %>

        <a href="main"><button class="btn">回到首页</button></a>
    </div>




    <script src="resources/js/jquery-3.3.1.min.js"></script>
    <script src="resources/js/bootstrap.min.js"></script>
</body>
</html>
