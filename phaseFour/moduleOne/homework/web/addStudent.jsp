<%@ page import="com.asia.bean.User" %><%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2020/10/26
  Time: 19:33
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>添加学生信息</title>
    <link rel="stylesheet" href="resources/css/bootstrap.min.css">
</head>

<style>

<%--    居中--%>
    #content {
        width: 800px;
        margin-right: auto;
        margin-left: auto;
        margin-top: 30px;
    }

</style>

<body>
<%--    防止没登录进入。。不知道为啥没用--%>
    <%
        if (session.getAttribute("user") ==null){
            out.print("<script language='JavaScript'>alert('请先登录！');window.location.href='login.jsp'</script>");
        }
    %>

    <div id="content">
        <%--标题--%>
        <div>

            <h2>欢迎使用学生管理系统，<%= ((User) session.getAttribute("user")).getUsername()%></h2>

        </div>

        <%--    添加信息提示     --%>
        <h3 style="color: red"><%= request.getAttribute("add") == null ? "":request.getAttribute("add") %></h3>

            <br>
        <a href="main.jsp"><button class="btn btn-success">回到首页</button></a>
            <br>
            <br>

<%--            添加学生信息表单--%>
        <form action="addStudent" method="post">
            <div class="form-group">
                <label>学号</label>
                <input type="text" class="form-control" placeholder="请输入学号" name="id" required="required">
            </div>
            <div class="form-group">
                <label>姓名</label>
                <input type="text" class="form-control" placeholder="请输入姓名" name="name" required="required">
            </div>
            <div class="form-group">
                <span>性别</span>
                <input type="radio" name="sex" value="男" checked="checked">男
                <input type="radio" name="sex" value="女">女
            </div>
            <div class="form-group">
                <label>出生日期</label>
                <input type="passowd" class="form-control" placeholder="请输入出生日期" name="birthday" required="required">
            </div>

            <div class="form-group">
                <label>邮箱</label>
                <input type="email" class="form-control" placeholder="请输入邮箱" name="mail" required="required">
            </div>

            <div class="form-group">
                <label for="comment">备注:</label>
                <textarea class="form-control" rows="5" id="comment" name="remark"></textarea>
            </div>
            <div class="form-group">
                <button class="btn btn-primary">提交</button>
            </div>
        </form>



    </div>




    <script src="resources/js/jquery-3.3.1.min.js"></script>
    <script src="resources/js/bootstrap.min.js"></script>
</body>
</html>
