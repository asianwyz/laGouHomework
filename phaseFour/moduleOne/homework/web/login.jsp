<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2020/10/26
  Time: 16:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>学生管理系统</title>
    <link rel="stylesheet" href="resources/css/bootstrap.min.css">
</head>

<style>

    .bgimg{
        position:fixed;
        top: 0;
        left: 0;
        width:100%;
        height:100%;
        min-width: 1000px;
        /*z-index:-10;*/
        zoom: 1;
        background-color: #fff;
        background: url(resources/img/background.jpg) no-repeat;
        background-size: cover;
        -webkit-background-size: cover;
        -o-background-size: cover;
        background-position: center 0;
    }

    #loginIn {
        width: 700px;
        height:400px;
        padding:100px;
        background-color: white;
        background: hsla(0, 0%, 100%, .3);
        border-radius: 20px;
        box-shadow: 0 10px 20px rgba(0, 0, 0, 0.7);
        /*z-index: 100;*/
        text-align: center;
        margin-top: 15%;
        margin-left: auto;
        margin-right: auto;
    }

    #inputs {
        margin-left: auto;
        margin-right: auto;
    }

</style>

<body>

    <div class="bgimg">

        <div id="loginIn">

            <h2>学生信息管理系统</h2>
            <br/>
            <form action="loginIn" method="post">
                <div id="inputs" class="input-group input-group-lg">
                    <p><input type="text" name="userName" class="form-control user" placeholder="请输入管理员用户名" size="25"/></p>
                    <br/>
                    <br/>
                    <p><input type="password" name="password" class="form-control user" placeholder="请输入管理员密码" size="25"/></p>
                </div>
                <br/>

                <br/>
                <br/>

                <p>
                    <input type="submit" id="put" value="登录" class="btn btn-primary" />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="reset" id="puts" value="重置" class="btn btn-danger" />
                </p>
            </form>
        </div>

    </div>


    <script src="resources/js/jquery-3.3.1.min.js"></script>
    <script src="resources/js/bootstrap.min.js"></script>
</body>
</html>
