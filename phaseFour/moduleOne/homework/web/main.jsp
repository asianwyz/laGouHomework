<%@ page import="com.asia.bean.User" %>
<%@ page import="com.asia.bean.Student" %>
<%@ page import="java.util.List" %>
<%@ page import="com.asia.service.StudentService" %><%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2020/10/26
  Time: 16:35
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>学生管理系统</title>
    <link rel="stylesheet" href="resources/css/bootstrap.min.css">
</head>

<style>

    #content {
        width: 1000px;
        margin-left: auto;
        margin-right: auto;
        margin-top: 30px;
    }

    .data, .page {
        margin-left: auto;
        margin-right: auto;
    }

    .page {
        width:500px;
    }

    .page a {
        margin : 0 10px;
    }

    .page span {
        font-size: large;
    }



</style>

<body>

    <% if (session.getAttribute("user") == null){
			out.print("<script language='JavaScript'>alert('请先登录！');window.location.href='login.jsp'</script>");
		}
    %>

    <div id="content">
        <%--标题--%>
        <div>

            <h2>欢迎使用学生管理系统，<%= ((User) session.getAttribute("user")).getUsername()%></h2>

        </div>

<%--            查询表单--%>
        <form action="check" method="post" onsubmit="return login()">
            <div class="form-group">
                <input id="checkInput" type="text" class="form-control" placeholder="请输入学号或姓名查询" name="check">
            </div>
            <div class="form-group">
                <button id="find" class="btn btn-primary">提交查询</button>
            </div>
        </form>
            <a href="addStudent.jsp"> <button class="btn btn-success">添加学生信息</button></a>

<%--        分页显示数据--%>
            <%
                // 获取学生service层
                StudentService studentService = new StudentService();
                int count = studentService.count(); // 获取总数据条数
                int pageSize=10;//每页显示多少条数据
                int totalpages = studentService.totalPage(pageSize); //最大页数
                String currentPage=request.getParameter("pageIndex"); //获得当前的页数，即第几页

//               如果当前页面为空设为1
                if(currentPage==null){
                    currentPage="1";
                }
                int pageIndex=Integer.parseInt(currentPage);
                //添加逻辑判断，防止页数异常
                if(pageIndex<1){
                    pageIndex=1;
                }else if(pageIndex>totalpages){
                    pageIndex=totalpages;
                }
                List<Student> students= studentService.getByPage(pageSize,pageIndex);  //返回特定页数的数据
            %>
        <%--  数据显示表格  --%>
        <table class="table">
            <caption >学生信息</caption>
            <thead>
                <tr>
                    <th>学号</th>
                    <th>姓名</th>
                    <th>性别</th>
                    <th>邮箱</th>
                    <th>生日</th>
                    <th>备注</th>
                    <th>操作</th>
                </tr>
            </thead>
            <tbody>
                <%
                    // 获取学生信息
                    for (Student stu : students) {
                %>

                <tr>
                    <th><%= stu.getId()%></th>
                    <th><%= stu.getName()%></th>
                    <th><%= stu.getSex()%></th>
                    <th><%= stu.getMail()%></th>
                    <th><%= stu.getBirthday()%></th>
                    <th><%= stu.getRemark()%></th>
                    <%--编辑栏--%>
                    <th><a href="update.jsp?id=<%=stu.getId()%>"> <button class="btn btn-danger">编辑</button></a>
                        <a href="delete?id=<%=stu.getId()%>"> <button class="btn btn-danger" onclick="return confirm('确定删除？')">删除</button></a></th>
                </tr>

                <% } %>


            </tbody>
        </table>

        <div class="data">
            <div class="page">
                <a href="main.jsp?pageIndex=1"><button class="btn btn-success ">首页</button></a>
                <a href="main.jsp?pageIndex=<%=pageIndex-1 %>"><button class="btn btn-success">上一页</button></a>
                <a href="main.jsp?pageIndex=<%=pageIndex+1 %>"><button class="btn btn-success">下一页</button></a>
                <a href="main.jsp?pageIndex=<%=totalpages%>"><button class="btn btn-success">末页</button></a>
                <br/>
                <h4 style="color:green; text-align: left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    当前页数:<%=pageIndex%>&nbsp;&nbsp;&nbsp;&nbsp;
                    总学生人数:<%=count%></h4>
            </div>

        </div>



    </div>

    <script src="resources/js/jquery-3.3.1.min.js"></script>
    <script src="resources/js/bootstrap.min.js"></script>
    <script>
        <%-- 如果为空不能查询 ，阻止查询表单提交--%>
        function login() {

            let check = $("#checkInput").val();
            // 校验是否为空
            if (check == "") {
                alert("请输入要查询的学生学号或姓名");
                return false;
            }

            return true;

        }

    </script>

</div>
</body>
</html>
