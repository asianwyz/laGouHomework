<%@ page import="com.asia.bean.User" %>
<%@ page import="com.asia.bean.Student" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2020/10/27
  Time: 13:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>学生管理系统</title>
    <link rel="stylesheet" href="resources/css/bootstrap.min.css">
</head>

<style>

    #content {
        width: 1000px;
        margin-left: auto;
        margin-right: auto;
        margin-top: 30px;
    }

</style>

<body>

    <%
        if (session.getAttribute("user") ==null){
            out.print("<script language='JavaScript'>alert('请先登录！');window.location.href='login.jsp'</script>");
        }
    %>

    <div id="content">


        <div>
            <h2>欢迎使用学生管理系统，<%= ((User) session.getAttribute("user")).getUsername()%></h2>
        </div>

        <%
            List<Student> checkStudents = (List<Student>) session.getAttribute("checkStudents");
            System.out.println("查询到的学生个数：" + checkStudents == null ? 0 : checkStudents.size());

            if (checkStudents == null || checkStudents.size() == 0) {
        %>

            <h3 style="color: red">
                没有查到有关学生信息
            </h3>

        <% } else { %>

            <table class="table">
                <caption>查询学生信息</caption>
                <thead>
                <tr>
                    <th>学号</th>
                    <th>姓名</th>
                    <th>性别</th>
                    <th>邮箱</th>
                    <th>生日</th>
                    <th>备注</th>
                </tr>
                </thead>
                <tbody>
                <%
                    for (Student stu : checkStudents) {
                %>

                <tr>
                    <th><%= stu.getId()%></th>
                    <th><%= stu.getName()%></th>
                    <th><%= stu.getSex()%></th>
                    <th><%= stu.getMail()%></th>
                    <th><%= stu.getBirthday()%></th>
                    <th><%= stu.getRemark()%></th>
                </tr>

                <% } %>


                </tbody>
            </table>

        <% } %>

        <a href="main.jsp"><button class="btn btn-success">回到首页</button></a>

    </div>

    <script src="resources/js/jquery-3.3.1.min.js"></script>
    <script src="resources/js/bootstrap.min.js"></script>

</body>
</html>
