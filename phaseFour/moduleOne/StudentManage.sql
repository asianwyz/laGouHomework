

-- 创建数据库
CREATE DATABASE web;
ALTER DATABASE web CHARACTER SET utf8;

-- 创建管理员表
CREATE TABLE t_user (
	username VARCHAR(20),
	PASSWORD VARCHAR(20)
);

-- 添加管理员账号
INSERT INTO t_user VALUES("admin", "123456");

-- 创建学生信息表
CREATE TABLE student (
	id VARCHAR(10) PRIMARY KEY, -- 学号
	NAME VARCHAR(20), -- 名字
	sex VARCHAR(4), -- 性别
	birthdy VARCHAR(20), -- 生日
	mail VARCHAR(20), -- 邮箱
	remark VARCHAR(300) -- 备注
);