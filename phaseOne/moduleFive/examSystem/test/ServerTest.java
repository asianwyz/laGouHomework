package com.asia.test;

import com.asia.server.ServerInitClose;

import java.io.IOException;

/**
 * created with IntelliJ IDEA.
 * date: 2020/6/6
 * time: 17:34
 *
 * @author: asiaw
 * Description:
 *      服务器测试类
 */
public class ServerTest {

    public static void main(String[] args) {
        ServerInitClose sic = null;
        try {
            // 1. 什么ServerInitClose类型的引用指向该类型的对象
            sic = new ServerInitClose();
            // 2. 调用成员方法来实现服务器的初始化操作
            sic.serverInit();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                sic.serverClose();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
