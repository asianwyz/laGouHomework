package com.asia.test;

import com.asia.client.ClientInitClose;
import com.asia.client.ClientView;
import com.asia.client.util.ClientScanner;

import java.io.IOException;

/**
 * created with IntelliJ IDEA.
 * date: 2020/6/6
 * time: 18:10
 *
 * @author: asiaw
 * Description:
 *      客户端测试类
 */
public class ClientTest {

    public static void main(String[] args) {
        ClientInitClose cic = null;
        try {
            // 1. 声明ClientInitClose类型的引用开启客户端
            cic = new ClientInitClose();
            // 2. 调用成员方法实现客户端的初始化操作
            cic.clientInit();
            // 3. 声明ClientView类型的引用指绘制主界面
            ClientView cv = new ClientView(cic);
            cv.clientMainPage();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                cic.clientClose();
                ClientScanner.closeScanner();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
