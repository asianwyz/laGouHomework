package com.asia.server;

import com.asia.model.UserMessage;
import com.asia.server.dao.ServerDao;

import java.io.IOException;

/**
 * created with IntelliJ IDEA.
 * date: 2020/6/6
 * time: 20:52
 *
 * @author: asiaw
 * Description:
 *      服务器主功能类
 */
public class ServerView {

    /**
     * 使用合成复用原则
     */
    private ServerInitClose sic;
    private ServerDao sd;

    /**
     * 通过构造方法实现
     * @param sic
     */
    public ServerView(ServerInitClose sic, ServerDao sd) {
        this.sic = sic;
        this.sd = sd;
    }

    public void serverReceive() throws IOException, ClassNotFoundException {
//        UserMessage message = (UserMessage) sic.getOis().readObject();
//        System.out.println(message);
//        // 调用方法实现管理员账号和密码信息的校验
//        if (sd.serverManagerCheck(message.getUser())) {
//            message.setType("success");
//        } else {
//            message.setType("fail");
//        }
//        sic.getOos().writeObject(message);
//        System.out.println("服务器发送校验成功");
    }

}
