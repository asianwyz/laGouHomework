package com.asia.server;

import com.asia.model.Student;
import com.asia.model.UserMessage;
import com.asia.server.dao.ProblemDao;
import com.asia.server.dao.ServerDao;
import com.asia.server.dao.StudentDao;

import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * created with IntelliJ IDEA.
 * date: 2020/6/6
 * time: 17:26
 *
 * @author: asiaw
 * Description:
 *      编程实现服务器的初始化和关闭
 */
public class ServerInitClose {

    /**
     * 自定义成员变量来记录Socket和流对象
     */
    private ServerSocket ss;
//    private Socket s;

    private StudentDao studentDao;
    private ProblemDao problemDao;

    /**
     * 自定义成员方法实现服务器的初始化操作
     */
    public void serverInit() throws IOException, ClassNotFoundException {
        // 准备文件存放地址
        fileInit();
        // 1. 创建大插排
        ss = new ServerSocket(8888);
        // 2. 等待客户端的连接请求，调用accept方法
        while (true) {
            System.out.println("等待客户端的连接请求，调用accept方法");
            Socket s = ss.accept();
            System.out.println("客户端连接成功！ ");
            // 3. 使用输入输出流进行通信
            ObjectInputStream ois = new ObjectInputStream(s.getInputStream());
            ObjectOutputStream oos = new ObjectOutputStream(s.getOutputStream());
            UserMessage message = (UserMessage) ois.readObject();
            System.out.println(message);
            // 调用方法实现管理员账号和密码信息的校验
            switch (message.getType()) {
                case "managerCheck":
                    managerLogin(message, ois, oos, s);
                    break;
                case "userCheck":
                    studentLogin(message, ois, oos, s);
                    break;
                default:
                    message.setType("fail");
                    System.out.println("不处理，结束这个小插排");
//                    oos.writeObject(message);
            }
        }
    }

    /**
     * 学员登录，成功进入学员系统服务器
     * @param message
     * @param ois
     * @param oos
     * @param s
     * @throws IOException
     */
    private void studentLogin(UserMessage message, ObjectInputStream ois, ObjectOutputStream oos, Socket s) throws IOException {
        if (studentDao.checkStudent(message.getUser())) {
            message.setType("success");
        } else {
            message.setType("fail");
        }
        oos.writeObject(message);
        System.out.println("服务器发送校验成功");
        if (message.getType().equals("success")) {
            new StudentServer(s, studentDao, problemDao, ois, oos, message.getUser().getUsername()).start();
        }
    }

    /**
     * 管理与登录，成功进入管理员系统服务器
     * @param message
     * @param ois
     * @param oos
     * @param s
     * @throws IOException
     */
    private void managerLogin(UserMessage message, ObjectInputStream ois, ObjectOutputStream oos, Socket s) throws IOException {
        if (ServerDao.serverManagerCheck(message.getUser())) {
            message.setType("success");
        } else {
            message.setType("fail");
        }
        oos.writeObject(message);
        System.out.println("服务器发送校验成功");
        if (message.getType().equals("success")) {
            new ManagerServer(s, studentDao, problemDao, ois, oos).start();
        }
    }

    /**
     * 初始化
     * @throws IOException
     */
    private void fileInit() throws IOException {
        File server = new File("d:/server");
        if (!server.exists()) {
            server.mkdir();
        }
        File studentsFile = new File("d:/server/students.txt");
        if (!studentsFile.exists()) {
            studentsFile.createNewFile();
        }
        File problemsFile = new File("d:/server/problems.txt");
        if (!problemsFile.exists()) {
            problemsFile.createNewFile();
        }
        studentDao = new StudentDao();
        studentDao.init();
        problemDao = new ProblemDao();
        problemDao.init();
    }


    /**
     * 自定义成员方法实现服务器的关闭操作
     */
    public void serverClose() throws IOException {
        // 4. 关闭socket并释放有关的资源
        ss.close();
        System.out.println("服务器成功关闭");
    }
}
