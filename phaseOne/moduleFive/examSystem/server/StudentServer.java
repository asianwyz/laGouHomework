package com.asia.server;

import com.asia.model.*;
import com.asia.server.dao.ProblemDao;
import com.asia.server.dao.StudentDao;

import java.io.*;
import java.net.Socket;
import java.util.List;
import java.util.Map;

/**
 * created with IntelliJ IDEA.
 * date: 2020/6/8
 * time: 19:19
 *
 * @author: asiaw
 * Description:
 *      学员线程类
 */
public class StudentServer extends Thread {

    private Socket socket;
    /**
     * 学员管理
     */
    private StudentDao studentDao;
    /**
     * 考题管理
     */
    private ProblemDao problemDao;
    private ObjectInputStream ois;
    private ObjectOutputStream oos;
    private final String studentUser = "StudentUser";
    private final String studentExam = "StudentExam";
    /**
     * 学员学号
     */
    private String id;

    /**
     * 成绩导出所在文件夹
     */
    private File file;

    public StudentServer(Socket socket, StudentDao studentDao, ProblemDao problemDao, ObjectInputStream ois, ObjectOutputStream oos, String id) {
        this.socket = socket;
        this.studentDao = studentDao;
        this.problemDao = problemDao;
        this.ois = ois;
        this.oos = oos;
        this.id = id;
        file = new File("d:/server/" + id);
        if (!file.exists()) {
            file.mkdir();
        }
    }

    @Override
    public void run() {
        System.out.println("进入学员服务器");
        boolean flag = true;
        while (flag) {
            try {
                UserMessage userMessage = (UserMessage) ois.readObject();
                switch (userMessage.getType()) {
                    case studentUser:
                        System.out.println("进入用户模块");
                        user();
                        System.out.println("回到学员总系统");
                        break;
                    case studentExam:
                        System.out.println("进入考试模块");
                        exam();
                        System.out.println("回到学员总系统");
                        break;
                    default:
                        System.out.println("退出学员服务器");
                        flag = false;
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        System.out.println("退出成功");
    }

    /**
     * 考试模块
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private void exam() throws IOException, ClassNotFoundException {
        boolean flag = true;
        while (flag) {
            StudentMessage studentMessage = (StudentMessage) ois.readObject();
            switch (studentMessage.getType()) {
                case "startExam":
                    startExam();
                    break;
                case "selectExam":
                    selectExam();
                    break;
                case "exportExam":
                    exportExam();
                    break;
                default:
                    flag = false;
                    exitStudent();
            }
        }
    }

    /**
     * 导出成绩
     * @throws IOException
     */
    private void exportExam() throws IOException {
        System.out.println("开始导出成绩");
        Student student = studentDao.selectId(id);
        System.out.println(file.getPath());
        File examFile = new File(file.getPath() + "/exam.txt");
        if (!examFile.exists()) {
            examFile.createNewFile();
        }
        System.out.println(examFile.getPath());
        BufferedWriter bw = new BufferedWriter(new FileWriter(examFile));
        bw.write("学号：" + student.getId() + "\n");
        bw.write("姓名：" + student.getName() + "\n");
        bw.write("成绩(按考试顺序)：" + student.getScores().toString() + "\n");
        bw.close();
        oos.writeObject(examFile.getPath());
        System.out.println("发送导出完成信息");
    }

    /**
     * 查询成绩
     * @throws IOException
     */
    private void selectExam() throws IOException {

        List<Integer> list = studentDao.selectId(id).getScores();
        oos.writeObject(list.size());

        for (Integer score : list) {
            oos.writeObject(score);
        }
    }

    /**
     * 开始考试
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private void startExam() throws IOException, ClassNotFoundException {
        Map<String, Problem> problems = problemDao.getProblems();
        oos.writeObject(problems.size());

        for (Problem problem : problems.values()) {
            oos.writeObject(problem);
        }

        Integer score = (Integer) ois.readObject();
        studentDao.addScore(id, score);

    }

    /**
     * 用户模块
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private void user() throws IOException, ClassNotFoundException {
        boolean flag = true;
        while (flag) {
            StudentUserMessage studentUserMessage = (StudentUserMessage) ois.readObject();
            switch (studentUserMessage.getType()) {
                case "updateName":
                    updateName(studentUserMessage);
                    break;
                case "updatePassword":
                    updatePassword(studentUserMessage);
                    break;
                default:
                    flag = false;
                    exitStudent();
            }
        }
    }

    /**
     * 退出用户模块
     * @throws IOException
     */
    private void exitStudent() throws IOException {
        studentDao.save();
    }

    /**
     * 修改用户密码
     * @param studentUserMessage
     * @throws IOException
     */
    private void updatePassword(StudentUserMessage studentUserMessage) throws IOException {
        String type = studentDao.updatePassword(id, studentUserMessage.getMessage());
        studentUserMessage.setType(type);
        oos.writeObject(studentUserMessage);
        System.out.println(studentDao.selectId(id));
    }

    /**
     * 修改用户名字
     * @param studentUserMessage
     * @throws IOException
     */
    private void updateName(StudentUserMessage studentUserMessage) throws IOException {
        String type = studentDao.updateName(id, studentUserMessage.getMessage());
        studentUserMessage.setType(type);
        oos.writeObject(studentUserMessage);
        System.out.println(studentDao.selectId(id));
    }

}
