package com.asia.server;

import com.asia.model.*;
import com.asia.server.dao.ProblemDao;
import com.asia.server.dao.StudentDao;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * created with IntelliJ IDEA.
 * date: 2020/6/8
 * time: 19:19
 *
 * @author: asiaw
 * Description:
 *      管理员线程类
 */
public class ManagerServer extends Thread {

    private Socket socket;
    /**
     * 学员管理
     */
    private StudentDao studentDao;
    /**
     * 考题管理
     */
    private ProblemDao problemDao;
    private ObjectInputStream ois;
    private ObjectOutputStream oos;
    private final String studentManage = "StudentManage";
    private final String problemManage = "ProblemManage";

    public ManagerServer(Socket socket, StudentDao studentDao, ProblemDao problemDao, ObjectInputStream ois, ObjectOutputStream oos) {
        this.socket = socket;
        this.studentDao = studentDao;
        this.problemDao = problemDao;
        this.ois = ois;
        this.oos = oos;
    }

    @Override
    public void run() {
        System.out.println("进入管理员服务器");
        boolean flag = true;
        while (flag) {
            try {
                UserMessage userMessage = (UserMessage) ois.readObject();
                switch (userMessage.getType()) {
                    case studentManage:
                        System.out.println("进入学员管理模块");
                        manageStudent();
                        System.out.println("回到管理员总系统");
                        break;
                    case problemManage:
                        System.out.println("进入考题管理模块");
                        manageProblem();
                        System.out.println("回到管理员总系统");
                        break;
                    default:
                        System.out.println("退出管理员服务器");
                        flag = false;
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        System.out.println("退出成功");
    }

    /**
     * 考题管理
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private void manageProblem() throws IOException, ClassNotFoundException {
        boolean flag = true;
        while (flag) {
            ProblemMessage problemMessage = (ProblemMessage) ois.readObject();
            switch (problemMessage.getType()) {
                case "addProblem":
                    addProblem(problemMessage);
                    break;
                case "deleteProblem":
                    deleteProblem(problemMessage);
                    break;
                case "selectProblem":
                    selectProblem(problemMessage);
                    break;
                case "selectAllProblems":
                    System.out.println("查询所有考题信息");
                    selectAllProblems();
                    break;
                case "updateProblem":
                    updateProblem(problemMessage);
                    break;
                default:
                    flag = false;
                    exitProblemMange();
            }
        }
    }

    /**
     * 修改考题
     * @param problemMessage
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private void updateProblem(ProblemMessage problemMessage) throws IOException, ClassNotFoundException {
        problemMessage = problemDao.selectProblem(problemMessage);
        oos.writeObject(problemMessage);
        if ("fail".equals(problemMessage.getType())) {
            System.out.println("修改失败");
            return;
        }
        Problem problem = (Problem) ois.readObject();
        String message = problemDao.updateProblem(problem);
        problemMessage.setType(message);
        oos.writeObject(problemMessage);
    }

    /**
     * 查询所有考题
     * @throws IOException
     */
    private void selectAllProblems() throws IOException {
        Map<String, Problem> problems = problemDao.selectAll();
        oos.writeObject(problems.size());

        for (Problem problem : problems.values()) {
            oos.writeObject(problem);
        }
    }

    /**
     * 退出考题管理模块
     * @throws IOException
     */
    private void exitProblemMange() throws IOException {
        problemDao.save();
    }

    /**
     * 查询单个考题
     * @param problemMessage
     * @throws IOException
     */
    private void selectProblem(ProblemMessage problemMessage) throws IOException {
        problemMessage = problemDao.selectProblem(problemMessage);
        System.out.println("查询" + problemMessage.getType());
        oos.writeObject(problemMessage);
    }

    /**
     * 删除考题
     * @param problemMessage
     * @throws IOException
     */
    private void deleteProblem(ProblemMessage problemMessage) throws IOException {
        String message = problemDao.deleteProblem(problemMessage.getProblem());
        problemMessage.setType(message);
        System.out.println("服务器：" + message);
        oos.writeObject(problemMessage);
    }

    /**
     * 添加考题
     * @param problemMessage
     * @throws IOException
     */
    private void addProblem(ProblemMessage problemMessage) throws IOException {
        String message = problemDao.addProblem(problemMessage.getProblem());
        problemMessage.setType(message);
        System.out.println("服务器：" + message);
        oos.writeObject(problemMessage);
    }

    /**
     * 学员信息管理
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private void manageStudent() throws IOException, ClassNotFoundException {
        boolean flag = true;
        while (flag) {
            StudentMessage studentMessage = (StudentMessage) ois.readObject();
            switch (studentMessage.getType()) {
                case "addStudent":
                    addStudent(studentMessage);
                    break;
                case "deleteStudent":
                    deleteStudent(studentMessage);
                    break;
                case "selectStudent":
                    selectStudent(studentMessage);
                    break;
                case "selectAllStudents":
                    selectAllStudents();
                    break;
                case "updateStudent":
                    updateStudent(studentMessage);
                    break;
                case "exit":
                    flag = false;
                    exitStudentManage();
                    break;
                default:
                    flag = false;
            }
        }
    }

    /**
     * 查询所有学员信息
     * @throws IOException
     */
    private void selectAllStudents() throws IOException {
        Map<String, Student> students = studentDao.selectAll();
        oos.writeObject(students.size());

        for (Student student : students.values()) {
            oos.writeObject(student);
        }
    }

    /**
     * 退出学员管理模块
     * @throws IOException
     */
    private void exitStudentManage() throws IOException {
        studentDao.save();
    }

    /**
     * 修改学员信息
     * @param studentMessage
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private void updateStudent(StudentMessage studentMessage) throws IOException, ClassNotFoundException {
        studentMessage = studentDao.selectStudent(studentMessage);
        oos.writeObject(studentMessage);
        if ("fail".equals(studentMessage.getType())) {
            System.out.println("修改失败");
            return;
        }
        Student student = (Student) ois.readObject();
        String message = studentDao.updateStudent(student);
        studentMessage.setType(message);
        oos.writeObject(studentMessage);
    }

    /**
     * 查询单个学员信息
     * @param studentMessage
     * @throws IOException
     */
    private void selectStudent(StudentMessage studentMessage) throws IOException {
        studentMessage = studentDao.selectStudent(studentMessage);
        System.out.println("查询" + studentMessage.getType());
        oos.writeObject(studentMessage);
    }

    /**
     * 删除学员信息
     * @param studentMessage
     * @throws IOException
     */
    private void deleteStudent(StudentMessage studentMessage) throws IOException {
        String message = studentDao.deleteStudent(studentMessage.getStudent());
        studentMessage.setType(message);
        System.out.println("服务器：" + message);
        oos.writeObject(studentMessage);
    }

    /**
     * 添加学员信息
     * @param studentMessage
     * @throws IOException
     */
    private void addStudent(StudentMessage studentMessage) throws IOException {
        String message = studentDao.addStudent(studentMessage.getStudent());
        studentMessage.setType(message);
        System.out.println("服务器：" + message);
        oos.writeObject(studentMessage);
    }
}
