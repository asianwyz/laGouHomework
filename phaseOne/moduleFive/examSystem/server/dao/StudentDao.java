package com.asia.server.dao;

import com.asia.model.Student;
import com.asia.model.StudentMessage;
import com.asia.model.User;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * created with IntelliJ IDEA.
 * date: 2020/6/8
 * time: 17:04
 *
 * @author: asiaw
 * Description:
 */
public class StudentDao {

    /**
     * 存放学生的map
     */
    private Map<String,Student> students;
    /**
     * 学员信息本地存放地址
     */
    private File file = new File("d:/server/students.txt");

    /**
     * 从本地读取学员信息
     * @throws IOException
     */
    public void init() throws IOException {
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            if (file.length() > 0) {
                students = (Map<String, Student>) ois.readObject();
            }
            else {
                students = new HashMap<>();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (null != ois) {
                ois.close();
            }
            if (null != fis) {
                fis.close();
            }
        }
    }

    public Map<String, Student> getStudents() {
        return students;
    }

    /**
     * 添加学员信息
     * @param student
     * @return
     */
    public String addStudent(Student student) {
        String id = student.getId();
        if (students.containsKey(id)) {
            return "已经有该学员的信息，添加失败";
        }
        students.put(id, student);
        System.out.println("已有学员" + students.size());
        return "添加学员成功";
    }

    /**
     * 删除学员信息
     * @param student
     * @return
     */
    public String deleteStudent(Student student) {
        String id = student.getId();
        if (students.containsKey(id)) {
            students.remove(id);
            System.out.println("删除学员" + id);
            return "删除学员" + id + "成功";
        } else {
            return "没有该学员信息，删除失败";
        }
    }

    /**
     * 查询学员信息
     * @param studentMessage
     * @return
     */
    public StudentMessage selectStudent(StudentMessage studentMessage) {
        String id = studentMessage.getStudent().getId();
        if (students.containsKey(id)) {
            studentMessage.setType("success");
            studentMessage.setStudent(students.get(id));
        }
        else {
            studentMessage.setType("fail");
        }
        return studentMessage;
    }

    /**
     * 修改学员信息
     * @param student
     * @return
     */
    public String updateStudent(Student student) {
        students.put(student.getId(), student);
        return "success";
    }

    /**
     * 将学员信息保存至本地
     * @throws IOException
     */
    public void save() throws IOException {
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;

        try {
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(students);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            oos.close();
            fos.close();
        }
    }

    /**
     * 查询所有学员信息
     * @return
     */
    public Map<String, Student> selectAll() {
        return students;
    }

    /**
     * 学员登录的密码检验
     * @param user
     * @return
     */
    public boolean checkStudent(User user) {
        String id = user.getUsername();
        if (!students.containsKey(id)) {
            return false;
        }
        String password = user.getPassword();
        return password.equals(students.get(id).getPassword());
    }

    /**
     * 修改学员姓名
     * @param id
     * @param name
     * @return
     */
    public String updateName(String id, String name) {
        students.get(id).setName(name);
        return "名字修改成功";
    }

    /**
     * 修改学员密码
     * @param id
     * @param password
     * @return
     */
    public String updatePassword(String id, String password) {
        students.get(id).setPassword(password);
        return "密码修改成功";
    }

    /**
     * 通过id查询学员信息
     * @param id
     * @return
     */
    public Student selectId(String id) {
        return students.get(id);
    }

    /**
     * 添加考试成绩
     * @param id
     * @param score
     */
    public void addScore(String id, Integer score) {
        students.get(id).getScores().add(score);
    }
}
