package com.asia.server.dao;

import com.asia.model.User;

/**
 * created with IntelliJ IDEA.
 * date: 2020/6/6
 * time: 21:18
 *
 * @author: asiaw
 * Description:
 */
public class ServerDao {

    /**
     * 管理员账号和密码校验
     * @param user
     * @return
     */
    public static boolean serverManagerCheck(User user) {
        return "admin".equals(user.getUsername()) && "123456".equals(user.getPassword());
    }
}
