package com.asia.server.dao;

import com.asia.model.Problem;
import com.asia.model.ProblemMessage;
import com.asia.model.Student;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * created with IntelliJ IDEA.
 * date: 2020/6/10
 * time: 10:59
 *
 * @author: asiaw
 * Description:
 *      考题curd类
 */
public class ProblemDao {

    /**
     * 考题存储的map
     */
    private Map<String, Problem> problems;

    /**
     * 考题本地存放地址
     */
    private File file = new File("d:/server/problems.txt");

    /**
     * 从本地读取考题
     * @throws IOException
     */
    public void init() throws IOException {
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            if (file.length() > 0) {
                fis = new FileInputStream(file);
                ois = new ObjectInputStream(fis);
                problems = (Map<String, Problem>) ois.readObject();
            }
            else {
                problems = new HashMap<>();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (null != ois) {
                ois.close();
            }
            if (null != fis) {
                fis.close();
            }
        }
    }

    public Map<String, Problem> getProblems() {
        return problems;
    }

    /**
     * 添加考题
     * @param problem
     * @return
     */
    public String addProblem(Problem problem) {
        String id = problem.getId();
        if (problems.containsKey(id)) {
            return "已经有该考题的信息，添加失败";
        }
        problems.put(id, problem);
        System.out.println("已有考题" + problems.size());
        return "添加考题成功";
    }

    /**
     * 删除考题
     * @param problem
     * @return
     */
    public String deleteProblem(Problem problem) {
        String id = problem.getId();
        if (problems.containsKey(id)) {
            problems.remove(id);
            System.out.println("删除考题" + id);
            return "删除考题" + id + "成功";
        }
        return "没有该考题编号，删除失败";
    }

    /**
     * 查询考题
     * @param problemMessage
     * @return
     */
    public ProblemMessage selectProblem(ProblemMessage problemMessage) {
        String id = problemMessage.getProblem().getId();
        if (problems.containsKey(id)) {
            problemMessage.setType("success");
            problemMessage.setProblem(problems.get(id));
        } else {
            problemMessage.setType("fail");
        }
        return problemMessage;
    }

    /**
     * 修改考题
     * @param problem
     * @return
     */
    public String updateProblem(Problem problem) {
        problems.put(problem.getId(), problem);
        return "修改成功";
    }

    /**
     * 保存至本地
     * @throws IOException
     */
    public void save() throws IOException {
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;

        try {
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(problems);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            oos.close();
            fos.close();
        }
    }

    /**
     * 显示所有考题
     */
    public void show() {
        for (Problem problem:problems.values()) {
            System.out.println(problem);
        }
    }

    /**
     * 查询所有考题
     * @return
     */
    public Map<String, Problem> selectAll() {
        return problems;
    }
}
