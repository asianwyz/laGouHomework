package com.asia.model;

import java.io.Serializable;

/**
 * created with IntelliJ IDEA.
 * date: 2020/6/8
 * time: 22:34
 *
 * @author: asiaw
 * Description:
 *      学员信息类
 */
public class StudentMessage implements Serializable {

    private static final long serialVersionUID = 6462949169060571383L;

    /**
     * 类型
     */
    private String type;

    private Student student;

    public StudentMessage(String type, Student student) {
        this.type = type;
        this.student = student;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }
}
