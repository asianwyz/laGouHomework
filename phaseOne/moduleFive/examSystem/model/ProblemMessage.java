package com.asia.model;

import java.io.Serializable;

/**
 * created with IntelliJ IDEA.
 * date: 2020/6/10
 * time: 10:58
 *
 * @author: asiaw
 * Description:
 *      题目信息类
 */
public class ProblemMessage implements Serializable {

    private static final long serialVersionUID = 5192712010115627042L;

    /**
     * 类型
     */
    private String type;

    /**
     * 考题
     */
    private Problem problem;

    public ProblemMessage(String type, Problem problem) {
        this.type = type;
        this.problem = problem;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Problem getProblem() {
        return problem;
    }

    public void setProblem(Problem problem) {
        this.problem = problem;
    }
}
