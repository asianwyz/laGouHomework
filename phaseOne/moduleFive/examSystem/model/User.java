package com.asia.model;

import java.io.Serializable;

/**
 * created with IntelliJ IDEA.
 * date: 2020/6/6
 * time: 19:49
 *
 * @author: asiaw
 * Description:
 *      用户类
 */
public class User implements Serializable {

    private static final long serialVersionUID = -6762728763441723648L;
    /**
     * 用于描述用户名
     */
    private String username;
    /**
     * 用于描述用户密码
     */
    private String password;



    public User() {
    }

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
