package com.asia.model;

import java.io.Serializable;
import java.util.Objects;

/**
 * created with IntelliJ IDEA.
 * date: 2020/6/10
 * time: 10:48
 *
 * @author: asiaw
 * Description:
 *      题目类
 */
public class Problem implements Serializable {

    private static final long serialVersionUID = -5271751743951753169L;

    /**
     * 题目id
     */
    private String id;

    /**
     * 题目内容
     */
    private String problemName;

    /**
     * 选项A
     */
    private String chooseA;

    /**
     * 选项B
     */
    private String chooseB;

    /**
     * 选项C
     */
    private String chooseC;

    /**
     * 选项D
     */
    private String chooseD;

    /**
     * 题目答案
     */
    private String answer;

    public Problem(String id) {
        this.id = id;
    }

    public Problem(String id, String problemName, String chooseA, String chooseB, String chooseC, String chooseD, String answer) {
        this.id= id;
        this.problemName = problemName;
        this.chooseA = chooseA;
        this.chooseB = chooseB;
        this.chooseC = chooseC;
        this.chooseD = chooseD;
        this.answer = answer;
    }

    public String getId() {
        return id;
    }

    public String getProblemName() {
        return problemName;
    }

    public void setProblemName(String problemName) {
        this.problemName = problemName;
    }

    public String getChooseA() {
        return chooseA;
    }

    public void setChooseA(String chooseA) {
        this.chooseA = chooseA;
    }

    public String getChooseB() {
        return chooseB;
    }

    public void setChooseB(String chooseB) {
        this.chooseB = chooseB;
    }

    public String getChooseC() {
        return chooseC;
    }

    public void setChooseC(String chooseC) {
        this.chooseC = chooseC;
    }

    public String getChooseD() {
        return chooseD;
    }

    public void setChooseD(String chooseD) {
        this.chooseD = chooseD;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public void show() {
        String str = problemName + "\n" +
                "A：" + chooseA + '\n' +
                "B：" + chooseB + '\n' +
                "C：" + chooseC + '\n' +
                "D：" + chooseD;
        System.out.println(str);
    }

    @Override
    public String toString() {
        return  "编号：" + id + "\n" +
                "题目：" + problemName + "\n" +
                "A：" + chooseA + '\n' +
                "B：" + chooseB + '\n' +
                "C：" + chooseC + '\n' +
                "D：" + chooseD + '\n' +
                "答案：" + answer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Problem problem = (Problem) o;
        return Objects.equals(problemName, problem.problemName) &&
                Objects.equals(chooseA, problem.chooseA) &&
                Objects.equals(chooseB, problem.chooseB) &&
                Objects.equals(chooseC, problem.chooseC) &&
                Objects.equals(chooseD, problem.chooseD) &&
                Objects.equals(answer, problem.answer);
    }

    @Override
    public int hashCode() {
        return Objects.hash(problemName, chooseA, chooseB, chooseC, chooseD, answer);
    }
}
