package com.asia.model;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * created with IntelliJ IDEA.
 * date: 2020/6/8
 * time: 17:05
 *
 * @author: asiaw
 * Description:
 *      学生类
 */
public class Student implements Serializable {

    private String id;
    private String name;
    private String password;
    private List<Integer> scores;

    public Student(String id, String password, String name) {
        this.id = id;
        this.password = password;
        this.name = name;
        this.scores = new LinkedList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Integer> getScores() {
        return scores;
    }

    public void setScores(Integer score) {
        scores.add(score);
    }

    @Override
    public String toString() {
        return "学号：" + id + "\n" +
               "密码：" + password + "\n" +
               "姓名：" + name + "\n" +
               "成绩：" + scores.toString();
    }
}
