package com.asia.model;

import java.io.Serializable;

/**
 * created with IntelliJ IDEA.
 * date: 2020/6/6
 * time: 19:51
 *
 * @author: asiaw
 * Description:
 *      用户消息类
 */
public class UserMessage implements Serializable {

    private static final long serialVersionUID = -4199658313936969028L;

    /**
     * 消息的类型代表具体的业务
     */
    private String type;

    /**
     * 消息的具体内容
     */
    private User user;

    public UserMessage() {
    }

    public UserMessage(String type, User user) {
        this.type = type;
        this.user = user;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "UserMessage{" +
                "type='" + type + '\'' +
                ", user=" + user +
                '}';
    }
}
