package com.asia.model;

import java.io.Serializable;

/**
 * created with IntelliJ IDEA.
 * date: 2020/6/10
 * time: 22:29
 *
 * @author: asiaw
 * Description:
 *      学员用户信息类
 */
public class StudentUserMessage implements Serializable {

    private static final long serialVersionUID = -6973337918985125398L;

    private String type;

    private String message;

    public StudentUserMessage(String type, String message) {
        this.type = type;
        this.message = message;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
