package com.asia.client.util;

import java.util.Scanner;

/**
 * created with IntelliJ IDEA.
 * date: 2020/6/6
 * time: 19:00
 *
 * @author: asiaw
 * Description:
 *      实现扫描器工具类的封装，可以在任意位置使用
 */
public class ClientScanner {

    private static Scanner sc = new Scanner(System.in);

    /**
     * 自定义成员方法实现扫描器的获取
     * @return
     */
    public static Scanner getSc() {
        return sc;
    }

    public static void closeScanner() {
        sc.close();
    }
}
