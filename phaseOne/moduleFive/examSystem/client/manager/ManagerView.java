package com.asia.client.manager;

import com.asia.client.ClientInitClose;
import com.asia.client.util.ClientScanner;
import com.asia.model.UserMessage;

import java.io.IOException;

/**
 * created with IntelliJ IDEA.
 * date: 2020/6/8
 * time: 15:52
 *
 * @author: asiaw
 * Description:
 *      管理员主界面的实现
 */
public class ManagerView {

    /**
     * 小插排
     */
    private ClientInitClose cic;

    /**
     * 学生管理主界面类
     */
    private StudentManagerView studentManagerView;

    /**
     * 考题管理主界面类
     */
    private ProblemManageView problemManageView;

    /**
     * 构造函数
     * @param cic
     */
    public ManagerView(ClientInitClose cic) {
        this.cic = cic;
        studentManagerView = new StudentManagerView(cic);
        problemManageView = new ProblemManageView(cic);
    }

    /**
     * 管理员主界面
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public void ManagerMainPage() throws IOException, ClassNotFoundException {
        // 标志
        boolean flag = true;
        while (flag) {
            System.out.println("  \n\n\t\t   管理员系统");
            System.out.println("-------------------------------------------");
            System.out.print("   [1] 学员管理模块");
            System.out.println("     [2] 考题管理模块");
            System.out.println("   [0] 退出系统");
            System.out.println("-------------------------------------------");
            System.out.println("请选择要进行的业务编号，其他任意输入视为退出管理员系统");
            String choose = ClientScanner.getSc().next();
            switch (choose) {
                case "1" :
                    System.out.println("进入学员管理模块");
                    studentManage();
                    break;
                case "2" :
                    System.out.println("进入考题管理模块");
                    problemManage();
                    break;
                default:
                    System.out.println("正在退出系统");
                    exit();
                    flag = false;
            }
        }
    }

    /**
     * 退出系统
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private void exit() throws IOException, ClassNotFoundException {
        UserMessage userMessage = new UserMessage("exit", null);
        cic.getOos().writeObject(userMessage);
    }

    /**
     * 进入考题管理界面
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private void problemManage() throws IOException, ClassNotFoundException {
        cic.getOos().writeObject(new UserMessage("ProblemManage", null));
        problemManageView.ProblemManagerMainPage();
    }

    /**
     * 进入学员管理界面
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private void studentManage() throws IOException, ClassNotFoundException {
        cic.getOos().writeObject(new UserMessage("StudentManage", null));
        studentManagerView.StudentManagerMainPage();
    }
}
