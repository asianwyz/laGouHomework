package com.asia.client.manager;

import com.asia.client.ClientInitClose;
import com.asia.client.util.ClientScanner;
import com.asia.model.StudentMessage;
import com.asia.model.StudentUserMessage;

import java.io.IOException;

/**
 * created with IntelliJ IDEA.
 * date: 2020/6/10
 * time: 21:52
 *
 * @author: asiaw
 * Description:
 *      学员用户类
 */
public class StudentUserView {

    /**
     * 小插排
     */
    private ClientInitClose cic;

    /**
     * 学员id
     */
    private String id;

    public StudentUserView(ClientInitClose cic, String id) {
        this.cic = cic;
        this.id = id;
    }

    /**
     * 学员用户主界面
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public void UserMainPage() throws IOException, ClassNotFoundException {
        boolean flag = true;
        while (flag) {
            System.out.println("  \n\n\t\t   用户模块");
            System.out.println("-------------------------------------");
            System.out.print("   [1] 修改名字");
            System.out.println("      [2] 修改密码");
            System.out.println("   [0] 退出");

            System.out.println("-------------------------------------");
            System.out.println("请选择要进行的业务编号，其他任意输入视为退出用户模块，回到学员系统");
            String choose = ClientScanner.getSc().next();
            switch (choose) {
                case "1":
                    updateName();
                    break;
                case "2":
                    updatePassword();
                    break;
                default:
                    System.out.println("退出用户模块，回到学员系统");
                    flag = false;
                    exit();

            }
        }
    }

    /**
     * 退出用户模块
     * @throws IOException
     */
    private void exit() throws IOException {
        StudentUserMessage studentUserMessage = new StudentUserMessage("exit", null);
        cic.getOos().writeObject(studentUserMessage);
    }

    /**
     * 修改密码
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private void updatePassword() throws IOException, ClassNotFoundException {
        System.out.println("请输入新密码：");
        String name = ClientScanner.getSc().next();
        StudentUserMessage studentUserMessage = new StudentUserMessage("updatePassword", name);
        cic.getOos().writeObject(studentUserMessage);
        studentUserMessage = (StudentUserMessage) cic.getOis().readObject();
        System.out.println(studentUserMessage.getType());
    }

    /**
     * 修改名字
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private void updateName() throws IOException, ClassNotFoundException {
        System.out.println("请输入新名字：");
        String name = ClientScanner.getSc().next();
        StudentUserMessage studentUserMessage = new StudentUserMessage("updateName", name);
        cic.getOos().writeObject(studentUserMessage);
        studentUserMessage = (StudentUserMessage) cic.getOis().readObject();
        System.out.println(studentUserMessage.getType());
    }
}
