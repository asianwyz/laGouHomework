package com.asia.client.manager;

import com.asia.client.ClientInitClose;
import com.asia.client.util.ClientScanner;
import com.asia.model.Problem;
import com.asia.model.Student;
import com.asia.model.StudentMessage;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * created with IntelliJ IDEA.
 * date: 2020/6/10
 * time: 22:13
 *
 * @author: asiaw
 * Description:
 *      学生考试主界面
 */
public class StudentExamView {

    /**
     * 小插排
     */
    private ClientInitClose cic;

    private String id;

    public StudentExamView(ClientInitClose cic, String id) {
        this.cic = cic;
        this.id = id;
    }

    /**
     * 考试主界面
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public void ExamMainPage() throws IOException, ClassNotFoundException {
        boolean flag = true;
        while (flag) {
            System.out.println("  \n\n\t\t   考试模块");
            System.out.println("-------------------------------------");
            System.out.print("   [1] 开始考试");
            System.out.println("      [2] 查询成绩");
            System.out.println("   [3] 导出成绩");

            System.out.println("-------------------------------------");
            System.out.println("请选择要进行的业务编号，其他任意输入视为退出考试模块，回到学员系统");
            String choose = ClientScanner.getSc().next();
            switch (choose) {
                case "1":
                    System.out.println("开始考试");
                    startExam();
                    break;
                case "2":
                    System.out.println("查询成绩");
                    selectExam();
                    break;
                case "3":
                    System.out.println("导出成绩");
                    exportExam();
                    break;
                default:
                    System.out.println("退出用户模块，回到学员系统");
                    flag = false;
                    exit();

            }
        }
    }

    /**
     * 导出成绩
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private void exportExam() throws IOException, ClassNotFoundException {
        StudentMessage studentMessage = new StudentMessage("exportExam", null);
        cic.getOos().writeObject(studentMessage);
        String path = (String) cic.getOis().readObject();
        System.out.println("成绩已导出至：" + path);
    }

    /**
     * 退出考试模块
     * @throws IOException
     */
    private void exit() throws IOException {
        StudentMessage studentMessage = new StudentMessage("exit", null);
        cic.getOos().writeObject(studentMessage);
    }

    /**
     * 查询成绩
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private void selectExam() throws IOException, ClassNotFoundException {

        StudentMessage studentMessage = new StudentMessage("selectExam", null);
        cic.getOos().writeObject(studentMessage);

        Integer size = (Integer) cic.getOis().readObject();
        System.out.println("总共考试了" + size + "次");
        for (int i = 1; i <= size ; i++) {
            Integer score = (Integer) cic.getOis().readObject();
            System.out.println("第" + i + "次得分为：" + score + "分");
        }

    }

    /**
     * 开始考试
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private void startExam() throws IOException, ClassNotFoundException {
        // 获取考题
        StudentMessage studentMessage = new StudentMessage("startExam", null);
        cic.getOos().writeObject(studentMessage);
        List<Problem> list = new LinkedList<>();
        Integer size = (Integer) cic.getOis().readObject();
        for (int i = 0; i < size; i++) {
            Problem problem = (Problem) cic.getOis().readObject();
            list.add(problem);
        }
        System.out.println("考题接收完毕");

        System.out.println("-----------------------考试开始---------------------");
        System.out.println("-----------------考题为多道单选题，一题5分-------------");

        int score = 0;
        String choose;
        for (Problem problem : list) {
            problem.show();
            System.out.println("请输入你的选项(A B C D)：");
            choose = ClientScanner.getSc().next();
            if (choose.equals(problem.getAnswer())) {
                score += 5;
            }
        }
        System.out.println("---------------------考试结束-----------------------");
        System.out.println("总分" + size * 5 + "，最终得分：" + score);
        cic.getOos().writeObject(score);
    }

}
