package com.asia.client.manager;

import com.asia.client.ClientInitClose;
import com.asia.client.util.ClientScanner;
import com.asia.model.Problem;
import com.asia.model.ProblemMessage;

import java.io.IOException;

/**
 * created with IntelliJ IDEA.
 * date: 2020/6/10
 * time: 10:41
 *
 * @author: asiaw
 * Description:
 *      考题管理主界面
 */
public class ProblemManageView {

    /**
     * 小插排
     */
    private ClientInitClose cic;

    public ProblemManageView(ClientInitClose cic) {
        this.cic = cic;
    }

    /**
     * 考题管理主界面
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public void ProblemManagerMainPage() throws IOException, ClassNotFoundException {
        boolean flag = true;
        while (flag) {
            System.out.println("  \n\n\t\t   考题管理模块");
            System.out.println("-------------------------------------");
            System.out.print("   [1] 增加考题");
            System.out.println("      [2] 修改考题");
            System.out.print("   [3] 删除考题");
            System.out.println("      [4] 查询考题");

            System.out.println("-------------------------------------");
            System.out.println("请选择要进行的业务编号，其他任意输入视为退出考题管理模块，回到管理员系统");
            String choose = ClientScanner.getSc().next();
            switch (choose) {
                case "1":
                    System.out.println("添加考题");
                    addProblem();
                    break;
                case "2":
                    System.out.println("修改考题");
                    updateProblem();
                    break;
                case "3":
                    System.out.println("删除考题");
                    deleteProblem();
                    break;
                case "4":
                    System.out.println("查询考题");
                    selectProblem();
                    break;
                default:
                    System.out.println("退出考题管理模块，回到管理员系统");
                    flag = false;
                    exit();
                    
            }
        }
    }

    /**
     * 退出考题管理模块
     * @throws IOException
     */
    private void exit() throws IOException {
        ProblemMessage problemMessage = new ProblemMessage("exit", null);
        cic.getOos().writeObject(problemMessage);
    }

    /**
     * 查询单个考题或者所有考题信息
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private void selectProblem() throws IOException, ClassNotFoundException {
        System.out.println("查询所有考题？输入 y 查询所有考题，输入其它查询单个考题");
        String select = ClientScanner.getSc().next();
        if ("y".equalsIgnoreCase(select)) {
            cic.getOos().writeObject(new ProblemMessage("selectAllProblems", null));
            // 接收考题信息
            Integer size = (Integer) cic.getOis().readObject();
            for (int i = 0; i < size; i++) {
                Problem problem = (Problem) cic.getOis().readObject();
                System.out.println(problem);
            }
            return;
        }

        System.out.println("请输入要查询的考题编号（输入后换行）");
        String id = ClientScanner.getSc().next();
        ProblemMessage problemMessage = new ProblemMessage("selectProblem", new Problem(id));
        cic.getOos().writeObject(problemMessage);
        problemMessage = (ProblemMessage) cic.getOis().readObject();
        if ("success".equals(problemMessage.getType())) {
            System.out.println(problemMessage.getProblem().toString());
        } else {
            System.out.println("查询失败，没有该编号考题信息");
        }
    }

    /**
     * 删除考题
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private void deleteProblem() throws IOException, ClassNotFoundException {
        System.out.println("请输入要删除的考题编号（输入后换行）：");
        String id = ClientScanner.getSc().next();
        ProblemMessage problemMessage = new ProblemMessage("deleteProblem", new Problem(id));
        cic.getOos().writeObject(problemMessage);
        problemMessage = (ProblemMessage) cic.getOis().readObject();
        System.out.println(problemMessage.getType());
    }

    /**
     * 更新考题
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private void updateProblem() throws IOException, ClassNotFoundException {
        System.out.println("请输入要修改的考题编号（输入后换行）：");
        String id = ClientScanner.getSc().next();
        ProblemMessage problemMessage = new ProblemMessage("updateProblem", new Problem(id));
        cic.getOos().writeObject(problemMessage);

        problemMessage = (ProblemMessage) cic.getOis().readObject();
        if ("fail".equals(problemMessage.getType())) {
            System.out.println("没有该编号考题信息，修改失败");
            return;
        }
        Problem problem = problemMessage.getProblem();
        System.out.println("原题目：");
        System.out.println(problem);

        System.out.println("原题目为：" + problem.getProblemName() + "，如需修改请输入新题目内容，不修改输入字母 n ");
        String name = ClientScanner.getSc().next();
        if (!"n".equalsIgnoreCase(name)) {
            problem.setProblemName(name);
        }

        System.out.println("原 A 选项内容为：" + problem.getChooseA() + "，如需修改请输入新的选项，不修改输入字母 n ");
        String chooseA = ClientScanner.getSc().next();
        if (!"n".equalsIgnoreCase(chooseA)) {
            problem.setChooseA(chooseA);
        }
        System.out.println("原 B 选项内容为：" + problem.getChooseB() + "，如需修改请输入新的选项，不修改输入字母 n ");
        String chooseB = ClientScanner.getSc().next();
        if (!"n".equalsIgnoreCase(chooseB)) {
            problem.setChooseB(chooseB);
        }
        System.out.println("原 C 选项内容为：" + problem.getChooseC() + "，如需修改请输入新的选项，不修改输入字母 n ");
        String chooseC = ClientScanner.getSc().next();
        if (!"n".equalsIgnoreCase(chooseC)) {
            problem.setChooseC(chooseC);
        }
        System.out.println("原 D 选项内容为：" + problem.getChooseD() + "，如需修改请输入新的选项，不修改输入字母 n ");
        String chooseD = ClientScanner.getSc().next();
        if (!"n".equalsIgnoreCase(chooseD)) {
            problem.setChooseD(chooseD);
        }
        System.out.println("原答案为：" + problem.getAnswer() + "，请输入新的答案（不变输入原答案）");
        String ans = ClientScanner.getSc().next();
        problem.setAnswer(ans);

        cic.getOos().writeObject(problem);

        problemMessage = (ProblemMessage) cic.getOis().readObject();
        System.out.println(problemMessage.getType());
    }

    /**
     * 添加考题
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private void addProblem() throws IOException, ClassNotFoundException {

        String id = getString("请输入考题编号（0以上的数字）：");
        String name = getString("请输入题目内容：");
        String chooseA = getChoose("A");
        String chooseB = getChoose("B");
        String chooseC = getChoose("C");
        String chooseD = getChoose("D");
        String ans = getString("请输入答案选项：");

        Problem problem = new Problem(id, name, chooseA, chooseB, chooseC, chooseD, ans);
        ProblemMessage problemMessage = new ProblemMessage("addProblem", problem);
        cic.getOos().writeObject(problemMessage);
        problemMessage = (ProblemMessage) cic.getOis().readObject();
        System.out.println(problemMessage.getType());
    }

    /**
     * 方便读取，可以在优化
     * @param a
     * @return
     */
    private String getChoose(String a) {
        System.out.println("请输入选项" + a + "内容：");
        return ClientScanner.getSc().next();
    }

    /**
     * 方便读取，可以在优化
     * @param str
     * @return
     */
    private String getString(String str) {
        System.out.println(str);
        return ClientScanner.getSc().next();
    }
}
