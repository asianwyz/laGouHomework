package com.asia.client.manager;

import com.asia.client.ClientInitClose;
import com.asia.client.util.ClientScanner;
import com.asia.model.Problem;
import com.asia.model.ProblemMessage;
import com.asia.model.Student;
import com.asia.model.StudentMessage;

import java.io.IOException;

/**
 * created with IntelliJ IDEA.
 * date: 2020/6/9
 * time: 18:01
 *
 * @author: asiaw
 * Description:
 *      学员管理模块类
 */
public class StudentManagerView {

    /**
     * 小插排
     */
    private ClientInitClose cic;

    /**
     * 学员初始密码
     */
    private final String password = "123456";

    /**
     * 构造函数
     * @param cic
     */
    public StudentManagerView(ClientInitClose cic) {
        this.cic = cic;
    }

    /**
     * 学员管理主界面
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public void StudentManagerMainPage() throws IOException, ClassNotFoundException {
        boolean flag = true;
        while (flag) {
            System.out.println("  \n\n\t\t   学员管理模块");
            System.out.println("-------------------------------------");
            System.out.print("   [1] 添加学员");
            System.out.println("      [2] 修改学员");
            System.out.print("   [3] 删除学员");
            System.out.println("      [4] 查询学员");

            System.out.println("-------------------------------------");
            System.out.println("请选择要进行的业务编号，其他任意输入视为退出学员管理模块，回到管理员系统");
            String choose = ClientScanner.getSc().next();
            switch (choose) {
                case "1":
                    System.out.println("添加学员");
                    addStudent();
                    break;
                case "2":
                    System.out.println("修改学员");
                    updateStudent();
                    break;
                case "3":
                    System.out.println("删除学员");
                    deleteStudent();
                    break;
                case "4":
                    System.out.println("查询学员");
                    selectStudent();
                    break;
                default:
                    System.out.println("退出学员管理模块，回到管理员系统");
                    flag = false;
                    exit();
            }
        }
    }

    /**
     * 修改学员信息
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private void updateStudent() throws IOException, ClassNotFoundException {

        System.out.println("请输入要修改的学员学号（数字1~100000000之间）");
        String id = ClientScanner.getSc().next();
        // 先看是否有该学号，再进行后面的修改
        StudentMessage studentMessage = new StudentMessage("updateStudent", new Student(id, password, null));
        cic.getOos().writeObject(studentMessage);

        studentMessage = (StudentMessage) cic.getOis().readObject();
        if ("fail".equals(studentMessage.getType())) {
            System.out.println("没有该学号信息，修改失败");
            return;
        }
        // 读取修改内容，发送给服务器
        Student student = studentMessage.getStudent();
        System.out.println("现有姓名为：" + student.getName() + "，如需修改姓名请输入新名字，不修改输入原姓名");
        String name = ClientScanner.getSc().next();
        System.out.println("现有密码为：" + student.getPassword() + "，如需修改密码请输入原密码，不修改输入原密码");
        String password = ClientScanner.getSc().next();
        student.setName(name);
        student.setPassword(password);
        cic.getOos().writeObject(student);

        studentMessage = (StudentMessage) cic.getOis().readObject();
        System.out.println(studentMessage.getType());
    }

    /**
     * 退出学员管理
     * @throws IOException
     */
    private void exit() throws IOException {
        StudentMessage studentMessage = new StudentMessage("exit", null);
        cic.getOos().writeObject(studentMessage);
    }

    /**
     * 查询单个学员信息和所有学生信息
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private void selectStudent() throws IOException, ClassNotFoundException {

        System.out.println("查询所有学生信息？输入 y 查询所有学生信息，输入其它查询单个学生信息");
        String select = ClientScanner.getSc().next();
        if ("y".equalsIgnoreCase(select)) {
            cic.getOos().writeObject(new StudentMessage("selectAllStudents", null));
            // 接收学员信息
            Integer size = (Integer) cic.getOis().readObject();
            for (int i = 0; i < size; i++) {
                Student student = (Student) cic.getOis().readObject();
                System.out.println(student);
            }
            return;
        }

        System.out.println("请输入要查询的学员学号（数字1~100000000之间）");
        String id = ClientScanner.getSc().next();
        StudentMessage studentMessage = new StudentMessage("selectStudent", new Student(id, password, null));
        cic.getOos().writeObject(studentMessage);
        studentMessage = (StudentMessage) cic.getOis().readObject();
        if ("success".equals(studentMessage.getType())) {
            System.out.println(studentMessage.getStudent().toString());
        } else {
            System.out.println("查询失败，没有该学号信息");
        }
    }

    /**
     * 删除学员信息
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private void deleteStudent() throws IOException, ClassNotFoundException {
        System.out.println("请输入要删除的学员学号（数字1~100000000之间）");
        String id = ClientScanner.getSc().next();
        StudentMessage studentMessage = new StudentMessage("deleteStudent", new Student(id, password, null));
        cic.getOos().writeObject(studentMessage);
        studentMessage = (StudentMessage) cic.getOis().readObject();
        System.out.println(studentMessage.getType());
    }

    /**
     * 添加学员
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private void addStudent() throws IOException, ClassNotFoundException {
        System.out.println("请输入要添加的新学员的学号（数字1~100000000之间）（学员初始密码统一设为“123456”）：");
        String id = ClientScanner.getSc().next();
        System.out.println("请输入新学员的姓名：");
        String name = ClientScanner.getSc().next();
        Student student = new Student(id, password, name);
        StudentMessage studentMessage = new StudentMessage("addStudent", student);
        cic.getOos().writeObject(studentMessage);
        studentMessage = (StudentMessage) cic.getOis().readObject();
        System.out.println(studentMessage.getType());
    }
}
