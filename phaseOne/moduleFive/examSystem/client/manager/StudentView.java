package com.asia.client.manager;

import com.asia.client.ClientInitClose;
import com.asia.client.util.ClientScanner;
import com.asia.model.UserMessage;

import java.io.IOException;

/**
 * created with IntelliJ IDEA.
 * date: 2020/6/10
 * time: 21:39
 *
 * @author: asiaw
 * D|escription:
 *      学员主界面实现类
 */
public class StudentView {

    /**
     * 小插排
     */
    private ClientInitClose cic;

    /**
     * 学员id
     */
    private String id;

    /**
     * 学生用户主界面
     */
    private StudentUserView studentUserView;

    /**
     * 学生考试主界面
     */
    private StudentExamView studentExamView;

    public StudentView(ClientInitClose cic, String id) {
        this.cic = cic;
        this.id = id;
        studentUserView = new StudentUserView(cic, id);
        studentExamView = new StudentExamView(cic, id);
    }

    /**
     * 学生系统主界面
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public void StudentMainPage() throws IOException, ClassNotFoundException {
        boolean flag = true;
        while (flag) {
            System.out.println("  \n\n\t\t   学员系统");
            System.out.println("-------------------------------------------");
            System.out.print("   [1] 用户模块");
            System.out.println("     [2] 考试模块");
            System.out.println("   [0] 退出系统");
            System.out.println("-------------------------------------------");
            System.out.println("请选择要进行的业务编号，其他任意输入视为退出学员系统");
            String choose = ClientScanner.getSc().next();
            switch (choose) {
                case "1" :
                    System.out.println("进入用户模块");
                    user();
                    break;
                case "2" :
                    System.out.println("进入考试模块");
                    exam();
                    break;
                default:
                    System.out.println("正在退出系统");
                    exit();
                    flag = false;
            }
        }
    }

    /**
     * 退出系统
     * @throws IOException
     */
    private void exit() throws IOException {
        UserMessage userMessage = new UserMessage("exit", null);
        cic.getOos().writeObject(userMessage);
    }

    /**
     * 进入考试模块
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private void exam() throws IOException, ClassNotFoundException {
        cic.getOos().writeObject(new UserMessage("StudentExam", null));
        studentExamView.ExamMainPage();
    }

    /**
     * 进入用户模块
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private void user() throws IOException, ClassNotFoundException {
        cic.getOos().writeObject(new UserMessage("StudentUser", null));
        studentUserView.UserMainPage();
    }

}
