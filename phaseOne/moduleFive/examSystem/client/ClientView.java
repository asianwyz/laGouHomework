package com.asia.client;

import com.asia.client.manager.ManagerView;
import com.asia.client.manager.StudentView;
import com.asia.model.User;
import com.asia.model.UserMessage;
import com.asia.client.util.ClientScanner;

import java.io.IOException;

/**
 * created with IntelliJ IDEA.
 * date: 2020/6/6
 * time: 18:20
 *
 * @author: asiaw
 * Description:
 *      变成实现客户端的主界面绘制和相应功能的实现
 */
public class ClientView {

    /**
     * 为了可以使用输入输出流，采用合成复用原则实现
     */
    private ClientInitClose cic;

    public ClientView(ClientInitClose cic) {
        this.cic = cic;
    }

    /**
     * 自定义成员方法实现客户端主界面的绘制
     */
    public void clientMainPage() throws IOException, ClassNotFoundException {
        System.out.println("  \n\n\t\t   在线考试系统");
        System.out.println("-------------------------------------------");
        System.out.print("   [1] 学员系统");
        System.out.println("     [2] 管理员系统");
        System.out.println("   [0] 退出系统");
        System.out.println("-------------------------------------------");
        System.out.println("请选择要进行的业务编号：");

        String choose = ClientScanner.getSc().next();
        switch (choose) {
            case "1":
                clientStudentLogin();
                break;
            case "2":
                clientManagerLogin();
                break;
            default:
                System.out.println("正在退出系统...");
                exit();
        }
    }

    /**
     * 自定义成员方法实现客户端学员登录的功能
     */
    private void clientStudentLogin() throws IOException, ClassNotFoundException {
        // 1. 准备学员登录的相关数据
        System.out.println("请输入学员的ID");
        String id = ClientScanner.getSc().next();
        System.out.println("请输入学员密码：");
        String password = ClientScanner.getSc().next();

        UserMessage userMessage = new UserMessage("userCheck", new User(id, password));

        // 2. 将UserMessage类型的对象通过对象输出流发送给服务器
        cic.getOos().writeObject(userMessage);
        System.out.println("客户端发送学员信息成功");

        // 3. 接收服务器的处理结果并给出提示
        userMessage = (UserMessage) cic.getOis().readObject();
        if ("success".equals(userMessage.getType())) {
            System.out.println("登录成功，进入学员系统！");
            StudentView sv = new StudentView(cic, id);
            sv.StudentMainPage();
        } else {
            System.out.println("用户名或密码错误");
        }
    }

    /**
     * 自定义成员方法实现客户端管理员登录的功能
     */
    private void clientManagerLogin() throws IOException, ClassNotFoundException {
        // 1. 准备管理员登录的相关数据
        System.out.println("请输入管理员的用户名");
        String userName = ClientScanner.getSc().next();
        System.out.println("请输入管理员的密码：");
        String password = ClientScanner.getSc().next();
        UserMessage userMessage = new UserMessage("managerCheck", new User(userName, password));
        // 2. 将UserMessage类型的对象通过对象输出流发送给服务器
        cic.getOos().writeObject(userMessage);
        System.out.println("客户端发送管理员信息成功");
        // 3. 接收服务器的处理结果并给出提示
        userMessage = (UserMessage) cic.getOis().readObject();
        if ("success".equals(userMessage.getType())) {
            System.out.println("登录成功，进入管理员系统！");
            ManagerView mv = new ManagerView(cic);
            mv.ManagerMainPage();
        } else {
            System.out.println("用户名或密码错误");
        }
    }

    /**
     * 退出
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private void exit() throws IOException, ClassNotFoundException {
        UserMessage userMessage = new UserMessage("exit", null);
        System.out.println("发送退出系统消息");
        cic.getOos().writeObject(userMessage);
//        System.out.println("等待接收确认退出系统消息");
//        userMessage = (UserMessage) cic.getOis().readObject();
//        System.out.println(userMessage.getType());
    }
}
