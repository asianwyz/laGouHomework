package com.asia.homework;

import java.io.File;

/**
 * created with IntelliJ IDEA.
 * date: 2020/5/22
 * time: 8:35
 *
 * @author: Administrator
 * Description:
 */
public class num02 {

    public static void main(String[] args) {
        delete(new File("d:/哈哈"));
    }

    /**
     * 删除file目录下的内容以及子目录下的内容
     * @param file
     */
    public static void delete(File file) {
        // 获取该目录下的所有内容
        File[] files = file.listFiles();
        // 遍历目录
        for (File f:files) {
            String name = f.getName();
            // 判断是否为文件，是直接删除
            if (f.isFile()) {
                System.out.println(f.delete() ? name + "文件删除成功" : name + "文件删除失败");
            }
            // 若是目录，就递归运行，然后删除该目录
            if (f.isDirectory()) {
                delete(f);
                System.out.println(f.delete() ? name + "目录删除成功" : name + "目录删除失败");
            }
        }
    }
}
