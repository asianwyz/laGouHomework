package com.asia.homework.num03;

import java.io.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * created with IntelliJ IDEA.
 * date: 2020/5/22
 * time: 20:04
 *
 * @author: Administrator
 * Description:
 *      3. 编程题
 *
 *      使用线程池将一个目录中的所有内容拷贝到另外一个目录中，包含子目录中的内容。
 */
public class Num03 {

    static ExecutorService executorService;

    public static void main(String[] args) {
        // 初始目录
        File file = new File("d:/哈哈");
        System.out.println(file.getPath());
        // 目的目录
        File fileCopy = new File("d:/呵呵");

        // 创建一个线程池
        executorService = Executors.newFixedThreadPool(10);
        // 拷贝目录
        copy(file, fileCopy);
        // 关闭线程池
        executorService.shutdown();
        System.out.println("线程池关闭");
    }

    public static void copy(File file, File fileCopy) {

        // 获取该目录下的所有内容
        File[] files = file.listFiles();
        // 遍历目录
        for (File f:files) {
            String name = f.getName();
            // 判断是否为文件，是文件，新开一个线程进行拷贝
            if (f.isFile()) {
                executorService.execute(new FileCopyRunnable(file, fileCopy, name));
            }
            // 若是目录，就递归运行，然后删除该目录
            if (f.isDirectory()) {
                File fc = new File(fileCopy.getPath() + "/" + name);
                fc.mkdir();
                copy(f, new File(fileCopy.getPath() + "/" + name));
                System.out.println(f.getName() + "子目录下的所有内容拷贝成功");
            }
        }
    }
}
