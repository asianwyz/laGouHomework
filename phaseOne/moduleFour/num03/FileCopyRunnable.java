package com.asia.homework.num03;

import java.io.*;

/**
 * created with IntelliJ IDEA.
 * date: 2020/5/22
 * time: 22:30
 *
 * @author: Administrator
 * Description:
 */
public class FileCopyRunnable implements Runnable{

    private File file;

    private File fileCopy;

    private String name;

    public FileCopyRunnable(File file, File fileCopy, String name) {
        this.file = file;
        this.fileCopy = fileCopy;
        this.name = name;
    }

    @Override
    public void run() {
        BufferedInputStream bis = null;
        BufferedOutputStream bos = null;
        try {
            bis = new BufferedInputStream(new FileInputStream(file.getPath() + "/" + name));
            bos = new BufferedOutputStream(new FileOutputStream(fileCopy.getPath() + "/" + name));
            System.out.println("正在拷贝" + name + "文件");
            byte[] barr = new byte[1024];
            int res = 0;
            while ((res = bis.read(barr)) != -1) {
                bos.write(barr, 0, res);
            }
            System.out.println(name + "文件拷贝成功。当前线程ID为：" + Thread.currentThread().getId());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (null != bos) {
                try {
                    bos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (null != bis) {
                try {
                    bis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
