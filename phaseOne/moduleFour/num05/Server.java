package com.asia.homework.num0510;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

/**
 * created with IntelliJ IDEA.
 * date: 2020/6/2
 * time: 9:26
 *
 * @author: asiaw
 * Description:
 *      聊天室服务器类
 */
public class Server {

    // 大插排
    static ServerSocket ss;

    // map，记录小插排和其对应的用户名
    static Map<String, Socket> map;

    // 服务器存放文件的地址
    static File fileLoad;

    // 静态代码块初始化
    static {
        try {
            // 初始化大插排
            ss = new ServerSocket(8778);
            // 创建文件存放地址
            fileLoad = new File("d:/server");
            if (!fileLoad.exists()) {
                fileLoad.mkdir();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        map = new HashMap<>();
    }

    public static void main(String[] args) {
        // 初始化服务器
        initServer();
    }

    // 初始化服务器
    private static void initServer() {

        System.out.println("聊天室服务器开启");

        // 服务器一直监听
        while (true) {
            Socket client = null;
            try {
                // 获取小插排
                client = ss.accept();

                // 接收用户名
                String name = getMessages(client);
                // 将该用户添加到当前在线用户map中
                map.put(name, client);

                String message = name + "上线了！聊天室目前共有" + map.size() + "人在线";
                System.out.println(message);
                // 将新用户上线告诉其他用户
                for (String str : map.keySet()) {
                    sendMessage(map.get(str), message);
                }
                // 给该新用户开一个线程单独管理该用户
                new ServerThread(client, name).start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    // 从小插排获取消息，不能关闭流，不然会有bug，？关闭流会关闭socket
    private static String getMessages(Socket client) {
        try {
            DataInputStream dis = new DataInputStream(client.getInputStream());
            return dis.readUTF();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    // 给小插排发送消息
    private static void sendMessage(Socket socket, String message) {
        try {
            DataOutputStream dos = new DataOutputStream(socket.getOutputStream());
            dos.writeUTF(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // 给用户开的单独线程类
    private static class ServerThread extends Thread {
        // 小插排
        Socket s;
        // 用户名
        String name;
        // 读取发送文件的IO流
        BufferedOutputStream bos;
        BufferedInputStream bis;
        // 发送消息的IO流
        DataOutputStream dos;
        DataInputStream dis;
        // 存放该用户发送的文件的地址
        File fileClient;

        // 初始化
        public ServerThread(Socket s, String name) {
            this.s = s;
            this.name = name;
            try {
                InputStream is = s.getInputStream();
                OutputStream os = s.getOutputStream();
                bos = new BufferedOutputStream(os);
                bis = new BufferedInputStream(is);
                dis = new DataInputStream(is);
                dos = new DataOutputStream(os);
                fileClient = new File(fileLoad.getPath() + "/" + name);
                if (!fileClient.exists()) {
                    fileClient.mkdir();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void run() {
            String message = null;
            // 获取小插排发过来的消息
            try {
                message = dis.readUTF();
            } catch (IOException e) {
                e.printStackTrace();
            }
            // 一直监听消息
            while (message != null) {
                try {
                    // 为bye，表示退出
                    if ("bye".equalsIgnoreCase(message)) {
                        break;
                    }
                    // 接收用户发过来的文件
                    if ("准备发送文件，请服务器准备接收".equals(message)) {
                        System.out.println("开始接收文件");
                        // 接收文件名
                        String fileName = dis.readUTF();
                        System.out.println("文件名：" + fileName);

                        // 准备存放文件
                        File receiveFile = new File(fileClient.getPath() + "/" + fileName);
                        if (!receiveFile.exists()) {
                            receiveFile.createNewFile();
                        }
                        // 存放文件的IO流
                        BufferedOutputStream os = new BufferedOutputStream(new FileOutputStream(receiveFile));
                        // 接收文件
                        int n = 512, len = 512;
                        byte[] data = new byte[n];//一次读取512个字节
                        while (len >= n) {
                            len = bis.read(data, 0, n);
                            os.write(data);
                        }
                        System.out.println("文件接收完毕");
                        os.close();
                        // 将文件传输给其他在线用户
                        for (String str : map.keySet()) {
                            if (str != name) {
                                // 文件发送类
                                new FileThread(map.get(str), receiveFile).start();
                            }
                        }
                    }
                    else {
                        // 将消息发送给其他在线用户
                        for (String str : map.keySet()) {
                            if (str != name) {
                                sendAllMessage(map.get(str), name + "说：" + message);
                            }
                        }
                    }
                    // 接收消息
                    message = dis.readUTF();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            // 用户下线，移出map
            map.remove(name);
            message = name + "下线了，聊天室还有" + map.size() + "人";
            System.out.println(message);
            // 告诉其他用户，该用户已经下线
            for (String str : map.keySet()) {
                if (str != name) {
                    sendAllMessage(map.get(str), message);
                }
            }
        }

        // 给其他小插排发送消息
        private void sendAllMessage(Socket socket, String message) {
            try {
                DataOutputStream dos = new DataOutputStream(socket.getOutputStream());
                dos.writeUTF(message);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    // 给用户发送文件的线程类
    private static class FileThread extends Thread {
        // 小插排
        Socket s;
        // 输出流
        BufferedOutputStream bos;
        DataOutputStream dos;
        // 要发送给用户的文件
        File file;

        public FileThread(Socket s, File file) {
            this.s = s;
            this.file = file;
            try {
                OutputStream os = s.getOutputStream();
                bos = new BufferedOutputStream(os);
                dos = new DataOutputStream(os);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void run() {
            try {
                // 发送提示信息，文件名，文件大小
                dos.writeUTF("准备发送文件，客户端请接收");
                dos.writeUTF(file.getName());
                dos.writeUTF(String.valueOf(file.length()));
                FileInputStream fis = new FileInputStream(file);

                int m, len;
                long l = file.length(); //获取文件长度，单位kb
                if (l % 512 != 0) {
                    len = m = 512;
                }
                else {
                    len = m = 511;  //防止文件正好是512字节的倍数，造成阻塞
                }
                // 从本地读取文件，发送给用户
                byte buffer[] = new byte[m];//读取输入流 ，一次读取512字节
                while (len >= m) {
                    len=fis.read(buffer,0,m);
                    bos.write(buffer,0,len);
                    bos.flush();
                }
                fis.close();
                System.out.println("文件读取发送成功！");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
