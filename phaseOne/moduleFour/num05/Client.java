package com.asia.homework.num0510;

import java.io.*;
import java.net.Socket;
import java.util.Arrays;
import java.util.Scanner;

/**
 * created with IntelliJ IDEA.
 * date: 2020/6/2
 * time: 9:27
 *
 * @author: asiaw
 * Description:
 */
public class Client {
    // 小插排
    static private Socket s;
    // 小插排的IO流
    static InputStream is;

    static OutputStream os;

    // 文件发送IO流
    static BufferedInputStream bis;

    static BufferedOutputStream bos;

    // 消息发送IO流
    static DataOutputStream dos;

    static DataInputStream dis;

    // 文件存放路径
    static File fileLoad;

    static File fileClient;

    // 初始化
    static {
        try {
            s = new Socket("127.0.0.1", 8778);
            is = s.getInputStream();
            os = s.getOutputStream();
            bos = new BufferedOutputStream(os);
            dos = new DataOutputStream(os);
            bis = new BufferedInputStream(is);
            dis = new DataInputStream(is);
            fileLoad = new File("d:/client");
            if (!fileLoad.exists()) {
                fileLoad.mkdir();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // 新开一个线程从服务器接收消息或者文件
    private static void getMessage() {
        new Thread(() -> {
            while (true) {
                try {
                    String message = dis.readUTF();
                    if (!"准备发送文件，客户端请接收".equals(message)) {
                        System.out.println(message);
                    }
                    else {
                        String fileName = dis.readUTF();
                        long length = Long.parseLong(dis.readUTF());
                        // 准备存放文件，生产名字和地址
                        int max=100000,min=1;
                        long randomNum = System.currentTimeMillis();
                        int rm = (int) (randomNum%(max-min)+min);
                        File receiveFile = new File(fileClient.getPath() + "/" + rm + fileName);
                        if (!receiveFile.exists()) {
                            receiveFile.createNewFile();
                        }

                        // 准备文件输出流
                        BufferedOutputStream os = new BufferedOutputStream(new FileOutputStream(receiveFile));
                        System.out.println("从服务器接收文件，命名为：" + receiveFile.getName());
                        // 从服务器接收文件，存入本地
                        int n = 512, len = 512;
                        if (length % 512 == 0) {
                            n = 511;
                            len = 511;
                        }
                        byte[] data = new byte[n];//一次读取512个字节
                        while (len >= n) {
                            len = bis.read(data, 0, n);
                            os.write(data);
                            os.flush();
                        }
                        System.out.println("文件接收完毕");
                        os.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    // 主线程
    public static void main(String[] args) {
        System.out.println("欢迎来到聊天室，请输入用户名，然后换行");
        Scanner in = new Scanner(System.in);
        // 读取用户名
        String username = in.nextLine();

        // 初始化接收的文件存放地址
        fileClient = new File(fileLoad.getPath() + "/" + username);
        if (!fileClient.exists()) {
            fileClient.mkdir();
        }
        // 将用户名发送给服务器
        try {
            dos.writeUTF(username);
        } catch (IOException e) {
            e.printStackTrace();
        }
        // 从服务器接收消息
        getMessage();
        // 给服务器发送消息或者文件
        while (true) {
            // 提示输入消息
            System.out.println("请输入聊天内容，如需发送文件，请输入file，输入单词 bye 退出聊天室");
            // 获取用户输入消息
            String message = in.nextLine();
            // 判断是否发送消息
            boolean flag = true;
            try {
                // 用户输入bye退出聊天室
                if ("bye".equalsIgnoreCase(message)) {
                    dos.writeUTF(message);
                    System.out.println("已经退出聊天室");
                    break;
                }
                // 发送文件
                if ("file".equalsIgnoreCase(message)) {
                    // 确认是否发送文件
                    System.out.println("输入 y 确认发送文件，其他输入将 file 当作消息发送");
                    String str = in.nextLine();
                    if ("y".equalsIgnoreCase(str)) {
                        flag = false;
                        // 获取文件地址
                        System.out.println("请输入要发送的文件地址");
                        String fileName = in.nextLine();
                        // 准备文件输入流
                        File file = new File(fileName);
                        FileInputStream fis = new FileInputStream(file);
                        // 给服务器发送提示信息
                        dos.writeUTF("准备发送文件，请服务器准备接收");
                        dos.writeUTF(file.getName());
                        // 向服务器发送文件
                        int m, len;
                        long l = file.length(); //获取文件长度，单位kb
                        if (l % 512 != 0) {
                            len = m = 512;
                        }
                        else {
                            len = m = 511;      //防止文件正好是512字节的倍数
                        }
                        byte buffer[] = new byte[m];
                        while (len >= m) {
                            len=fis.read(buffer,0,m);
                            bos.write(buffer,0,len);
                            bos.flush();
                        }
                        fis.close();

                        System.out.println("文件读取发送成功！");
                    }

                }
                if (flag) {
                    dos.writeUTF(message);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        // 结束
        System.exit(-1);
    }
}
