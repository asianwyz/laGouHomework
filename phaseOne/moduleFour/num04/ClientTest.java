package com.asia.homework.num04;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

/**
 * created with IntelliJ IDEA.
 * date: 2020/5/22
 * time: 18:47
 *
 * @author: Administrator
 * Description:
 *      客户端测试类
 */
public class ClientTest {

    public static void main(String[] args) {
        // 定义小插排
        Socket s = null;
        Scanner in = new Scanner(System.in);

        try {
            s = new Socket("127.0.0.1", 8888);

            // 让用户输入账号和密码
            System.out.println("请输入账号名：");
            String username = in.next();
            System.out.println("请输入密码：");
            String password = in.next();

            // 封装用户
            User user = new User(username, password);
            UserMessage um = new UserMessage(user, "check");

            // 将用户信息传给服务端
            OutputStream out = s.getOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(out);
            oos.writeObject(um);

            // 从服务端接收信息
            InputStream is = s.getInputStream();
            ObjectInputStream ois = new ObjectInputStream(is);
            UserMessage getUM = (UserMessage) ois.readObject();

            // 判断登录是否成功
            String isSuccess = getUM.getIsSuccess();
            System.out.println("success".equals(isSuccess) ? "登录成功" : "登录失败");

        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (null != s) {
                try {
                    s.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
