package com.asia.homework.num04;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * created with IntelliJ IDEA.
 * date: 2020/5/22
 * time: 11:17
 *
 * @author: Administrator
 * Description:
 *      服务端测试类
 */
public class ServerTest {

    public static void main(String[] args) {
        // 定义大插排
        ServerSocket ss = null;
        // 定义小插排
        Socket s = null;

        try {
            // 创建ServerSocket类型的对象并提供端口号
            ss = new ServerSocket(8888);
            // 等待客户端的连接请求，调用accept方法
            // 好像只需要一次。就不要while啦
//            while (true) {
            System.out.println("等待客户端的连接请求");
            // 没有客户端连接时服务器阻塞在accept方法的调用这里
            s = ss.accept();
            System.out.println("客户端" + s.getInetAddress() + "连接成功。");

            // 接收客户端传过来的对象
            // 先获取当前套接字的输入流
            InputStream in = s.getInputStream();
            ObjectInputStream ois = new ObjectInputStream(in);
            // 获取客户端传过来的用户信息
            UserMessage um = (UserMessage) ois.readObject();
            // 获取用户
            User user = um.getUser();
            // 判断账号密码是否正确
            if ("admin".equals(user.getUsername()) && "123456".equals(user.getPassword())) {
                um.setIsSuccess("success");
            }
            else {
                um.setIsSuccess("fail");
            }
            // 准备输出流
            OutputStream out = s.getOutputStream();
            // 准备对象输出流
            ObjectOutputStream oos = new ObjectOutputStream(out);
            // 输出
            oos.writeObject(um);

//            }
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        finally {
            // 关闭Socket并释放资源
            if (null != ss) {
                try {
                    System.out.println("服务器关闭");
                    ss.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
