package com.asia.homework.num04;

import java.io.Serializable;

/**
 * created with IntelliJ IDEA.
 * date: 2020/5/22
 * time: 11:09
 *
 * @author: Administrator
 * Description:
 *      用户信息类
 */
public class UserMessage implements Serializable {

    /**
     * 用户
     */
    private User user;

    /**
     *  登录是否成功的标志
     */
    private String isSuccess;

    public UserMessage(User user, String isSuccess) {
        this.user = user;
        this.isSuccess = isSuccess;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getIsSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(String isSuccess) {
        this.isSuccess = isSuccess;
    }
}
