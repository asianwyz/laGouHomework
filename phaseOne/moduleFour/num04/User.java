package com.asia.homework.num04;

import java.io.Serializable;

/**
 * created with IntelliJ IDEA.
 * date: 2020/5/22
 * time: 11:07
 *
 * @author: Administrator
 * Description:
 *      用户类
 */
public class User implements Serializable {

    /**
     * 用户名
     */
    private String username;

    /**
     * 用户密码
     */
    private String password;

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
