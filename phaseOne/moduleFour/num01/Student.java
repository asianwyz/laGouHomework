package com.asia.homework.num01;

import java.io.Serializable;
import java.util.Objects;

/**
 * created with IntelliJ IDEA.
 * date: 2020/5/21
 * time: 10:22
 *
 * @author: asiaw
 * Description:
 *      学生信息类
 */
public class Student implements Serializable {

    /**
     * 学号
     */
    private int id;

    /**
     * 姓名
     */
    private String name;

    /**
     * 年龄
     */
    private int age;

    public Student() {
    }

    public Student(int id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        if (id > 0 && id < Integer.MAX_VALUE) {
            this.id = id;
        } else {
            //System.out.println("年龄不合理哦！！！");
            try {
                throw new StudentIdException("学号不合理哦！！！");
            } catch (StudentIdException e) {
                e.printStackTrace();
            }
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        if (age > 0 && age < 150) {
            this.age = age;
        } else {
            //System.out.println("年龄不合理哦！！！");
            try {
                throw new StudentAgeException("年龄不合理哦！！！");
            } catch (StudentAgeException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Student student = (Student) o;
        return age == student.age &&
                Objects.equals(id, student.id) &&
                Objects.equals(name, student.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, age);
    }

    @Override
    public String toString() {
        return "Student{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
