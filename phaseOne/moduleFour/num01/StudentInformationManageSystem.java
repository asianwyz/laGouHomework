package com.asia.homework.num01;

import java.io.*;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Map;
import java.util.Scanner;

/**
 * created with IntelliJ IDEA.
 * date: 2020/5/21
 * time: 20:22
 *
 * @author: asiaw
 * Description:
 *   学生信息管理系统类
 */
public class StudentInformationManageSystem {


    /**
     * 存储学生信息的Map集合
     */
    private Map<Integer, Student> students = new HashMap<>();

    /**
     * 存储学生信息的txt文件
     */
    private File file = new File("d:/StudentInfo.txt");

    /**
     * 输出流
     */
    private FileOutputStream fos;
//
//    {
//        try {
//            fos = new FileOutputStream("d:/StudentInfo.txt");
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }
//    }

    /**
     * 输入流
     */
    private FileInputStream fis;

//    {
//        try {
//            fis = new FileInputStream("d:/StudentInfo.txt");
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }
//    }

    /**
     * 读取用户输入的选择
     */
    private Scanner in = new Scanner(System.in);

    /**
     * 进入系统提示信息输出
     */
    private void showWelcome() {
        System.out.println("******************************************************");
        System.out.println("**************** 欢迎进入学生信息管理系统 ****************");
        System.out.println("******************* 正在加载学生信息 ********************");
        // 开机从本地加载学生信息
        loadStudentInfo();
        System.out.println("*********************** 加载完毕 ***********************");
    }

    /**
     * 从本地加载学生信息
     */
    private void loadStudentInfo() {
        if (file.exists()) {
            ObjectInputStream ois = null;
            try {
                fis = new FileInputStream("d:/StudentInfo.txt");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            try {
                ois = new ObjectInputStream(fis);
                students = (Map<Integer, Student>) ois.readObject();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } finally {
                if (null != ois) {
                    try {
                        ois.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    /**
     * 操作提示信息输出
     */
    private void showMain() {
        System.out.println("1. 录入学生信息");
        System.out.println("2. 查询学生信息");
        System.out.println("3. 更改学生信息");
        System.out.println("4. 删除学生信息");
        System.out.println("5. 打印所有学生信息");
        System.out.println("其他任意输入：退出");
    }

    /**
     * 选择提示信息输出
     */
    private void showChoose() {
        System.out.println("请输入你的选择（输入数字1-5中的一个选择操作或其他任意输入选择退出）：");
    }

    /**
     * 系统的入口
     */
    public void show() {
        // 欢迎
        showWelcome();
        // 操作
        showAfter();
    }

    /**
     * 进入系统后的界面，获取操作
     */
    private void showAfter() {
        showMain();
        showChoose();
        getChoose();
    }

    /**
     * 新的操作提示信息
     */
    private void showNew() {
        System.out.println("请选择新的操作");
    }

    /**
     * 进行一次操作后显示的提示信息
     */
    private void showAfterOperation() {
        showNew();
        showAfter();
    }

    /**
     * 获取用户的操作选择
     */
    private void getChoose() {
        try {
            int choose = in.nextInt();
            operate(choose);
        }
        catch (Exception e) {
            exit();
        }
    }

    /**
     * 根据用户的选择进行相应的操作
     * @param choose 选择
     */
    private void operate(int choose) {
        switch (choose) {
            case 1 : setStudent(); break;
            case 2 : selectStudent(); break;
            case 3 : updateStudent(); break;
            case 4 : deleteStudent(); break;
            case 5 : printStudent(); break;
            default: exit();
        }
    }

    /**
     * 获取输入的学生姓名
     * @return
     */
    private String getName() {
        System.out.println("请输入学生的姓名");
        return in.next();
    }

    /**
     * 获取输入的学生年龄
     * @return
     */
    private int getAge() {
        int age = 0;
        while (true) {
            try {
                age = new Scanner(System.in).nextInt();
                if (age > 0 && age < 150) {
                    break;
                } else {
                    //System.out.println("年龄不合理哦！！！");
                    try {
                        throw new StudentAgeException("年龄不合理哦");
                    } catch (StudentAgeException e) {
                        e.printStackTrace();
                        System.out.println("请重新输入年龄，在数字1~150之间");
                    }
                }
            }
            catch (InputMismatchException ex) {
                try {
                    throw new StudentAgeException("年龄不合理哦");
                } catch (StudentAgeException e) {
                    e.printStackTrace();
                    System.out.println("请重新输入年龄，在数字1~150之间");
                }
            }
        }
        return age;
    }

    /**
     * 获取输入的学生学号
     * @return
     */
    private int getId() {
        int id = 0;
        while (true) {
            try {
                id = new Scanner(System.in).nextInt();
                if (id > 0 && id < 1000000000) {
                    break;
                } else {
                    //System.out.println("年龄不合理哦！！！");
                    try {
                        throw new StudentIdException("学号不合理哦！！！请重新输入，学号在数字1~1000000000之间");
                    } catch (StudentIdException e) {
                        e.printStackTrace();
                        System.out.println("请重新输入，学号在数字1~1000000000之间");
                    }
                }
            }
            catch (InputMismatchException ex) {
                try {
                    throw new StudentIdException("年龄不合理哦！请重新输入合法数字学号");
                } catch (StudentIdException e) {
                    e.printStackTrace();
                    System.out.println("请重新输入，学号在数字1~1000000000之间");
                }
            }
        }
        return id;
    }

    /**
     * 打印所有学生信息
     */
    private void printStudent() {
        System.out.println("打印所有学生信息");
        // 遍历打印
        for (Student st : students.values()) {
            System.out.println(st);
        }
        System.out.println("所有学生信息已经打印");
        showAfterOperation();
    }

    /**
     * 删除操作
     */
    private void deleteStudent() {
        System.out.println("请输入要删除信息学生的学号：");

        // 获取学号，判断是否有，有删除该学号信息
        int id = getId();

        if (students.containsKey(id)) {
            students.remove(id);
            System.out.println("已成功删除学号" + id + "的学生信息");
        }
        else {
            System.out.println("没有这个学号的学生信息");
        }
        showAfterOperation();
    }

    /**
     * 更新操作
     */
    private void updateStudent() {
        System.out.println("请输入要更新信息的学生学号");

        // 获取学号，是否有该学号信息，有读取更新的学号信息并更改
        int id = getId();
        if (!students.containsKey(id)) {
            System.out.println("没有该学号的学生信息");
        }
        else {
            System.out.println("请输入要更新的姓名（不变输入原姓名 " + students.get(id).getName() + " ）：");
            String name = getName();
            System.out.println("请输入要更新的年龄（不变输入原年龄 " + students.get(id).getAge() + " ）：");
            int age = getAge();
            // 更新
            students.put(id, new Student(id, name, age));
            System.out.println("学生信息更新完毕");
        }
        showAfterOperation();
    }

    /**
     * 查询操作
     */
    private void selectStudent() {
        System.out.println("请输入要查询的学生学号：");
        // 获取要查询的学生学号，有输出信息
        int id = getId();
        if (students.containsKey(id)) {
            System.out.println(students.get(id));
        }
        else {
            System.out.println("没有该学号的学生信息");
        }
        showAfterOperation();
    }

    /**
     * 录入操作
     */
    private void setStudent() {
        System.out.println("请输入学生的学号(数字1-100000000)");
        // 读取要录入的学生信息，然后录入
//        int id = in.nextInt();
        int id = getId();
        if (students.containsKey(id)) {
            System.out.println("该学号已经存在");
        }
        else {
            String name = getName();
            System.out.println("请输入学生的年龄");
            int age = getAge();
            students.put(id, new Student(id, name, age));
            System.out.println("学号" + id + "的学生信息已经录入");
        }
        showAfterOperation();
    }


    /**
     * 保存学生信息后退出
     */
    private void exit() {
        System.out.println("保存学生信息到d:/StudentInfo.txt中");
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        ObjectOutputStream oos = null;
        try {
            fos = new FileOutputStream("d:/StudentInfo.txt");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            oos = new ObjectOutputStream(fos);
            oos.writeObject(students);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (null != oos) {
                try {
                    oos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        System.out.println("学生信息保存完毕");
        System.out.println("退出系统，再见");
        
    }
}
