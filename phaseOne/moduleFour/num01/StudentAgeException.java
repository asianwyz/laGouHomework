package com.asia.homework.num01;

/**
 * created with IntelliJ IDEA.
 * date: 2020/5/21
 * time: 10:22
 *
 * @author: asiaw
 * Description:
 *      学生年龄异常类
 */
public class StudentAgeException extends Exception {

    private static final long serialVersionUID = 6931404082387756380L;

    public StudentAgeException() {
    }

    public StudentAgeException(String message) {
        super(message);
    }
}
