package com.asia.homework.num01;

/**
 * created with IntelliJ IDEA.
 * date: 2020/5/24
 * time: 22:29
 *
 * @author: Administrator
 * Description:
 */
public class StudentSystemTest {

    public static void main(String[] args) {
        System.out.println("开始演示");
        // 生成一个学生信息管理系统类
        StudentInformationManageSystem sys = new StudentInformationManageSystem();

        // 系统入口
        sys.show();
    }
}
