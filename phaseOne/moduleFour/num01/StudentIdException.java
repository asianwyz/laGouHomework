package com.asia.homework.num01;

/**
 * created with IntelliJ IDEA.
 * date: 2020/5/21
 * time: 10:22
 *
 * @author: asiaw
 * Description:
 *      学生年龄异常类
 */
public class StudentIdException extends Exception {

    private static final long serialVersionUID = 2635079058026025887L;

    public StudentIdException() {
    }

    public StudentIdException(String message) {
        super(message);
    }
}
