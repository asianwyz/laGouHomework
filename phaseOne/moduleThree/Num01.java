package com.asia.homework;

import java.util.Scanner;

/**
 * created with IntelliJ IDEA.
 * date: 2020/5/10
 * time: 12:26
 *
 * @author: asiaw
 * Description:
 *      编程统计字符串"ABCD123!@#$%ab"中大写字母、小写字母、数字、其它字符的个数并打 印出来。
 */
public class Num01 {

    public static void main(String[] args) {

        // 统计大写字母、小写字母、数字、其他字符的变量
        int countUp = 0, countLow = 0, countNumber = 0, countOther = 0;

        // 请输入一行字符串
        System.out.println("输入一行字符串");
        // 读取字符串
        Scanner in = new Scanner(System.in);
        String str = in.nextLine();

        // 遍历字符串  判读字符类别然后计数
        for (char ch:str.toCharArray()) {
            // 是否是大写字母
            if (ch >= 'A' && ch <= 'Z') {
                countUp++;
            }
            // 是否是小写字母
            else if (ch >= 'a' && ch <= 'z') {
                countLow++;
            }
            // 是否是数字
            else if (ch >= '0' && ch <= '9') {
                countNumber++;
            }
            // 其他字符
            else {
                countOther++;
            }
        }

        print("大写字母",countUp);
        print("小写字母",countLow);
        print("数字",countNumber);
        print("其他字符",countOther);

    }

    /**
     * 输出统计数目
     * @param str       统计项目
     * @param count     统计数目
     */
    private static void print(String str, int count) {
        System.out.println("字符串中" + str + "的数目是：" + count);
    }
}
