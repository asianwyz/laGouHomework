package com.asia.homework.num04;

/**
 * created with IntelliJ IDEA.
 * date: 2020/5/10
 * time: 16:10
 *
 * @author: asiaw
 * Description:
 *      4. 编程题
 *
 *      使用 List 集合实现简易的学生信息管理系统，要求打印字符界面提示用户选择相应的功能，
 *      根据用户输入的选择去实现增加、删除、修改、查找以及遍历所有学生信息的功能。
 *
 *      其中学生的信息有：学号、姓名、年龄。 要求： 尽量将功能拆分为多个.java 文件。
 */
public class Num04 {

}
