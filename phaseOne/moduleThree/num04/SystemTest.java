package com.asia.homework.num04;

/**
 * created with IntelliJ IDEA.
 * date: 2020/5/10
 * time: 17:34
 *
 * @author: asiaw
 * Description:
 *      学生信息管理系统测试类
 */
public class SystemTest {

    public static void main(String[] args) {
        // 生成一个学生信息管理系统类
        StudentInformationManageSystem sys = new StudentInformationManageSystem();

        // 系统入口
        sys.show();
    }
}






