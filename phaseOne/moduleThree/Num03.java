package com.asia.homework;

import java.util.HashMap;
import java.util.Map;

/**
 * created with IntelliJ IDEA.
 * date: 2020/5/10
 * time: 15:54
 *
 * @author: asiaw
 * Description:
 */
public class Num03 {

    public static void main(String[] args) {
        String s = "123,456,789,123,456";

        // 分隔字符串
        String[] str = s.split(",");

        // 存储次数
        Map<String, Integer> map = new HashMap<>(16);

        // 存储数字字符串
        for (String st : str) {
            // 已经有了就加一
            if (map.containsKey(st)) {
                map.put(st, map.get(st) + 1);
            }
            // 没有就存进去
            else {
                map.put(st, 1);
            }
        }

        // 输出
        for (String st : map.keySet()) {
            System.out.println(st + "出现了" + map.get(st) + "次");
        }
    }
}
