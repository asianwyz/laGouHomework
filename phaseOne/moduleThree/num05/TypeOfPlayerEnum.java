package com.asia.homework.num05;

/**
 * created with IntelliJ IDEA.
 * date: 2020/5/11
 * time: 10:49
 *
 * @author: asiaw
 * Description:
 *      玩家类型，农民还是地主
 */
public enum TypeOfPlayerEnum {
    /**
     * 农民和地主
     */
    PENSANT("农民"),LANDLORD("地主");

    private String type;

    TypeOfPlayerEnum(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
