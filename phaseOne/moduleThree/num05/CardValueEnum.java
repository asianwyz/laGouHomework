package com.asia.homework.num05;

/**
 * created with IntelliJ IDEA.
 * date: 2020/5/11
 * time: 7:18
 *
 * @author: asiaw
 * Description:
 *      牌大小的枚举
 */
public enum CardValueEnum {

    /**
     * 扑克牌的值，从大到小：大王，小王，2，A，K，Q，J，10，9，8，7，6，5，4，3
     */
    BIG("大王"),SMALL("小王"),TWO("2"),A("A"),K("K"),Q("Q"),J("J"),TEN("10"),
    NINE("9"),EIGHT("8"),SEVEN("7"),SIX("6"),FIVE("5"),FOUR("4"),THREE("3");

    /**
     * 牌大小的
     */
    private String value;

    CardValueEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
