package com.asia.homework.num05;

import java.util.Objects;

/**
 * created with IntelliJ IDEA.
 * date: 2020/5/10
 * time: 21:39
 *
 * @author: asiaw
 * Description:
 *      表示一张扑克牌
 */
public class Card implements Comparable{

    /**
     * 扑克牌的花色，黑红梅方或者大小王
     */
    private CardSuitEnum cardSuit;

    /**
     * 扑克牌值
     */
    private CardValueEnum cardValue;

    public Card(CardSuitEnum cardSuit, CardValueEnum cardValue) {
        this.cardSuit = cardSuit;
        this.cardValue = cardValue;
    }

    public CardSuitEnum getCardSuit() {
        return cardSuit;
    }

    public CardValueEnum getCardValue() {
        return cardValue;
    }

    @Override
    public String toString() {
        if (cardValue.equals(CardValueEnum.BIG) || cardValue.equals(CardValueEnum.SMALL)) {
            return cardValue.getValue();
        }
        else {
            return cardSuit.getCardSuit() + cardValue.getValue();
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {return true;}
        if (o == null || getClass() != o.getClass()) {return false;}
        Card card = (Card) o;
        return cardSuit.compareTo(card.cardSuit) == 0 &&
                cardValue.compareTo(card.cardValue) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(cardSuit, cardValue);
    }

    /**
     * 继承比较接口
     * @param o card
     * @return
     */
    @Override
    public int compareTo(Object o) {
        Card card = (Card) o;
        int cp = cardValue.compareTo(card.cardValue);
        if (cp == 0) {
            return cardSuit.compareTo(card.cardSuit);
        }
        else {
            return cp;
        }
    }
}
