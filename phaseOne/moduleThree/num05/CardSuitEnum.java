package com.asia.homework.num05;

/**
 * created with IntelliJ IDEA.
 * date: 2020/5/10
 * time: 21:35
 *
 * @author: asiaw
 * Description:
 *      扑克牌花色枚举类
 */
public enum CardSuitEnum {
    /**
     * SPADE：黑桃，HEART：红桃，CLUB：梅花，DIAMOND：方块
     */
    SPADE("黑桃"),HEART("红桃"),CLUB("梅花"),DIAMOND("方块");

    /**
     * 扑克牌花色
     */
    private String cardSuit;

    CardSuitEnum(String cardSuit) {
        this.cardSuit = cardSuit;
    }

    public String getCardSuit() {
        return cardSuit;
    }
}
