package com.asia.homework.num05;

import java.util.Scanner;

/**
 * created with IntelliJ IDEA.
 * date: 2020/5/11
 * time: 7:55
 *
 * @author: asiaw
 * Description:
 *      斗地主游戏控制类
 */
public class PensantsVsLandlordControl {

    /**
     * 游戏需要一副扑克牌
     */
    static private Poker poker;

    /**
     * 发给玩家的牌数
     */
    public static final int COUNTOFPLAYER = 17;

    /**
     * 玩家一
     */
    private Player playerOne;
    /**
     * 玩家二
     */
    private Player playerTwo;
    /**
     * 玩家三
     */
    private Player playerThree;

    {
        poker = new Poker();
    }

    /**
     * 生成一副新的牌
     */
    public void setPoker() {
        poker = new Poker();
    }

    /**
     * 性感荷官，在线发牌
     */
    public void deal() {
        showDealer();
        for (int i = 0; i < COUNTOFPLAYER; i++) {
            playerOne.setCards(poker.deal());
            playerTwo.setCards(poker.deal());
            playerThree.setCards(poker.deal());
        }
        System.out.println("发牌完毕");
    }

    /**
     * 查看底牌
     */
    public void showLastThreeCards() {
        System.out.println("最后三张底牌是：");
        poker.print();
    }

    /**
     * 游戏入口
     */
    public void begin() {
        System.out.println("********************");
        System.out.println("欢迎来到沙雕斗地主游戏");
        System.out.println("********************");
        setPlayer();
    }

    /**
     * 读取玩家姓名
     */
    public void setPlayer() {
        try {
            System.out.println("请输入三个玩家的名字，用空格隔开（请按要求书写。。。不然报错。。程序GG）");
            Scanner in = new Scanner(System.in);
            String str = in.nextLine();
            String[] names = str.split(" ");
            playerOne = new Player(names[0]);
            playerTwo = new Player(names[1]);
            playerThree = new Player(names[2]);
            deal();
            showPlayerCard(playerOne);
            showPlayerCard(playerTwo);
            showPlayerCard(playerThree);
            showLastThreeCards();
        }
        catch (Exception e) {
            System.out.println("名字输入错误，再见了少年，这游戏不适合你");
        }
    }

    /**
     * 显示玩家的牌
     * @param player
     */
    private void showPlayerCard(Player player) {
        System.out.println("玩家" + player.getName() + "的牌为：");
        player.print();
    }

    /**
     * show性感荷官
     */
    private void showDealer() {
        System.out.println("            性感荷官，在线发牌                   ");
        System.out.println("                                .::::.");
        System.out.println("                              .::::::::.");
        System.out.println("                              :::::::::::");
        System.out.println("                              ':::::::::::..");
        System.out.println("                               :::::::::::::::'");
        System.out.println("                                ':::::::::::.");
        System.out.println("                                  .::::::::::::::'");
        System.out.println("                                .:::::::::::...");
        System.out.println("                               ::::::::::::::''");
        System.out.println("                   .:::.       '::::::::''::::");
        System.out.println("                 .::::::::.      ':::::'  '::::");
        System.out.println("                .::::':::::::.    :::::    '::::.");
        System.out.println("              .:::::' ':::::::::. :::::      ':::.");
        System.out.println("            .:::::'     ':::::::::.:::::       '::.");
        System.out.println("          .::::''         '::::::::::::::       '::.");
        System.out.println("         .::''              '::::::::::::         :::...");
        System.out.println("      ..::::                  ':::::::::'        .:' ''''");
        System.out.println("   ..''''':'                    ':::::.'");
    }
}
