package com.asia.homework.num05;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * created with IntelliJ IDEA.
 * date: 2020/5/10
 * time: 21:17
 *
 * @author: asiaw
 * Description:
 *      玩家类
 */
public class Player {

    /**
     * 有多少张牌
     */
    private int countOfCard;

    /**
     * 玩家拥有的牌
     */
    private List<Card> cards;

    /**
     * 玩家类型，地主还是农民
     */
    public TypeOfPlayerEnum type;

    /**
     * 玩家名字
     */
    private String name;

    public Player(String name) {
        this.name = name;
        cards = new LinkedList<>();
        countOfCard = 0;
    }

    public String getName() {
        return name;
    }

    public TypeOfPlayerEnum getType() {
        return type;
    }

    public void setType(TypeOfPlayerEnum type) {
        this.type = type;
    }

    /**
     * 查看牌
     */
    public void print() {
//        Collections.sort(cards, (Card o1, Card o2) -> {});
        Collections.sort(cards);
        for (Card card : cards) {
            System.out.print(card + " ");
        }
        System.out.println();
    }

    /**
     * 抓牌
     * @param card 牌
     */
    public void setCards(Card card) {
        cards.add(card);
        countOfCard++;
    }
}
