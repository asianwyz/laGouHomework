package com.asia.homework.num05;

/**
 * created with IntelliJ IDEA.
 * date: 2020/5/11
 * time: 7:56
 *
 * @author: asiaw
 * Description:
 *      斗地主游戏测试类
 */
public class GameTest {

    public static void main(String[] args) {
        // 游戏控制类
        PensantsVsLandlordControl game = new PensantsVsLandlordControl();

        // 游戏开始
        game.begin();
    }
}


