package com.asia.homework.num05;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * created with IntelliJ IDEA.
 * date: 2020/5/10
 * time: 21:39
 *
 * @author: asiaw
 * Description:
 * 扑克牌类，表示一副扑克牌
 */
public class Poker {

    /**
     * 装54张扑克牌的集合
     */
    static List<Card> cards = new LinkedList<>();

    /**
     * 初始化一副牌
     */
    {
        // 加上大小王
        cards.add(new Card(null, CardValueEnum.BIG));
        cards.add(new Card(null, CardValueEnum.SMALL));

        // 加花色牌
        for (CardSuitEnum cs : CardSuitEnum.values()) {
            for (CardValueEnum cv : CardValueEnum.values()) {
                if (cv.equals(CardValueEnum.BIG) || cv.equals(CardValueEnum.SMALL)) {
                    continue;
                }
                cards.add(new Card(cs, cv));
            }
        }
        // 打乱牌序
        shuffle();
    }

    /**
     * 打乱牌的顺序
     */
    private void shuffle() {
        Collections.shuffle(cards);
    }

    /**
     * 查看牌（可以用来查看牌底）
     */
    public void print() {
        for (Card card : cards) {
            System.out.print(card + " ");
        }
        System.out.println();
    }

    /**
     * 发牌
     */
    public Card deal() {
        return cards.remove(0);
    }

}
