package com.asia.homework;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * created with IntelliJ IDEA.
 * date: 2020/5/10
 * time: 14:19
 *
 * @author: asiaw
 * Description:
 *      编程获取两个指定字符串中的最大相同子串。
 *
 *      如： s1="asdafghjka", s2="aaasdfg" 他们的最大子串为"asd"
 *
 *      提示： 将短的那个串进行长度依次递减的子串与较长的串比较。
 *
 */
public class Num02 {

    /**
     * 解题思路：f[i][j] 表示字符串str1 以下标 i 结尾的子串和str2 以下标j 结尾的子串相同的最大长度
     * @param args
     */
    public static void main(String[] args) {

        // 读取两个字符串
        Scanner in = new Scanner(System.in);
        System.out.println("请输入字符串1（将读取一行）：");
        String str1 = in.nextLine();
        System.out.println("请输入字符串2（将读取一行）：");
        String str2 = in.nextLine();

        // 建立动规数组f[i][j]
        int n = str1.length();
        int m = str2.length();
        int[][] f = new int[n + 1][m + 1];

        // 保存最大长度，初始值为0
        int count = 0;
        // 保存子串，初始为空
        List<String> list = new ArrayList<>();

        /**
         * str1.charAt(i) = str2.charAt(j)  : f[i][j] = f[i - 1][j - 1] + 1;
         */
        for (int i = 1; i <= n; i++) {
            char ch1 = str1.charAt(i - 1);
            for (int j = 1; j <= m; j++) {
                char ch2 = str2.charAt(j - 1);
                if (ch1 == ch2) {
                    f[i][j] = f[i - 1][j - 1] + 1;
                    // 如果当前子串长度大于记录最大长度，则更新最大长度，记录子串
                    if (count < f[i][j]) {
                        count = f[i][j];
                        list.clear();
                        list.add(str1.substring(i - count, i));
                    }
                    // 如果当前子串长度等于记录最大长度，则记录子串
                    else if (count == f[i][j]) {
                        list.add(str1.substring(i - count, i));
                    }
                }
            }

        }

        // 输出最大相同子串
        System.out.println("两个字符串的最大相同子串长度为：" + count);
        if (list.size() > 0) {
            System.out.println("长度为" + count + "的子串有" + list.size() + "个，如下所示：");
            System.out.println(list.toString());
        }
    }
}
