package phaseOne.moduleOne;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: asiaw
 * Date: 2020/4/25
 * Time: 8:58
 * Description:
 *      实现双色球抽奖游戏中奖号码的生成，中奖号码由 6 个红球号码和 1 个蓝球号码组成。
 *      其中红球号码要求随机生成 6 个 1~33 之间不重复的随机号码。 其中蓝球号码要求随机生成 1 个 1~16 之间的随机号码。
 */
public class num03 {
    public static void main(String[] args) {
        StringBuilder stringBuilder = new StringBuilder();
        Random rd = new Random();
        Set<Integer> set = new HashSet<>();
        for (int i = 0; i < 6; i++) {
            int red = rd.nextInt(33) + 1;
            while (set.contains(red)) {
                red = rd.nextInt(33) + 1;
            }
            stringBuilder.append("红" + red + " ");
            set.add(red);
        }
        int blue = rd.nextInt(16) + 1;
        stringBuilder.append("蓝" + blue);
        System.out.println(stringBuilder);
    }
}
