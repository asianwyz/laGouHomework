package phaseOne.moduleOne;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: asiaw
 * Date: 2020/4/25
 * Time: 10:37
 * Description:
 *      使用二维数组和循环实现五子棋游戏棋盘的绘制
 */
public class num05 {
    public static void main(String[] args) {
        char[][] ch = new char[17][17];
        char[] tr = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
        ch[0][0] = ' ';
        for (int i = 1; i < 17; i++) {
            ch[0][i] = tr[i - 1];
        }
        for (int i = 1; i < 17; i++) {
            ch[i][0] = tr[i - 1];
            for (int j = 1; j < 17; j++) {
                ch[i][j] = '+';
            }
        }
        for (int i = 0; i < ch.length; i++) {
            for (int j = 0; j < ch[i].length; j++) {
                System.out.print(ch[i][j] + " ");
            }
            System.out.println();
        }
    }
}
