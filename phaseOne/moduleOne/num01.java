package phaseOne.moduleOne;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: asiaw
 * Date: 2020/4/25
 * Time: 8:41
 * Description:
 *      提示用户输入年月日信息，判断这一天是这一年中的第几天并打印。
 */
public class num01 {
    public static void main(String[] args) {
        int y, m, d;
        int[] arr = {31,28,31,30,31,30,31,31,30,31,30,31};

        System.out.println("请输入三个整数，第一个整数表示年份，第二个整数表示月份，第三个整数表示日期");
        System.out.println("请输入年份：");
        Scanner in = new Scanner(System.in);
        y = in.nextInt();
        System.out.println("请输入月份：");
        m = in.nextInt();
        System.out.println("请输入日期：");
        d = in.nextInt();
        int count = d;
        for (int i = 0; i < m - 1; i++) {
            count += arr[i];
        }
        if (m > 2 && (y % 4 == 0 && y % 100 != 0)) {
            count++;
        }
        System.out.println(y + "年" + m + "月" + d + "日是一年中的第" + count + "天");

    }
}
