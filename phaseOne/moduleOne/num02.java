package phaseOne.moduleOne;

/**
 * Created with IntelliJ IDEA.
 * User: asiaw
 * Date: 2020/4/25
 * Time: 8:52
 * Description:
 *      编程找出 1000 以内的所有完数并打印出来。 所谓完数就是一个数恰好等于它的因子之和，如：6=1＋2＋3
 */
public class num02 {
    public static void main(String[] args) {
        int n = 1001;
        for (int i = 2; i < n ; i++) {
            int count = getFactorsCount(i);
            if (count == i) {
                System.out.println(i);
            }
        }
    }

    private static int getFactorsCount(int x) {
        int count = 1;
        for (int i = 2; i <= Math.sqrt(x); i++) {
            if (x % i == 0) {
                count += i;
                count += x / i;
            }
        }
        return count;
    }
}
