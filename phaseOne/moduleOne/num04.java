package phaseOne.moduleOne;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * User: asiaw
 * Date: 2020/4/25
 * Time: 10:04
 * Description:
 *      自定义数组扩容规则，当已存储元素数量达到总容量的 80%时，扩容 1.5 倍。
 *      例如，总容量是 10，当输入第 8 个元素时，数组进行扩容，容量从 10 变 15。
 */

class IntArrayExtend{
    private int size;
    private int useCount = 0;
    private int[] arr;
    private final double rate = 0.8;
    private final int DEFAULT_CAPACITY = 10;

    IntArrayExtend() {
        arr = new int[DEFAULT_CAPACITY];
        size = DEFAULT_CAPACITY;
    }

    IntArrayExtend(int size) {
        this.size = size;
        arr = new int[size];
    }

    /**
     * 获取数组容量
     * @return
     */
    public int size() {
        return size;
    }

    /**
     * 查找value在数组中的索引，没有返回-1
     * @param value
     * @return
     */
    public int contains(int value) {
        for (int i = 0; i < useCount; i++) {
            if (value == arr[i]) {
                return i;
            }
        }
        return -1;
    }

    /**
     * 根据索引返回值
     * @param index
     * @return
     */
    public int getIndex(int index) {
        if (index >= size || index < 0) {
            System.out.println("索引超过数组范围");
            return -1;
        }
        if (index < useCount) {
            return arr[index];
        }
        else {
            System.out.print("索引 " + index + " 处还没有进行赋值，初始值为");
            return 0;
        }
    }

    /**
     * 设置index索引处的值
     * @param index
     * @param vaule
     */
    public void setIndex(int index, int vaule) {
        if (index >= size || index < 0) {
            System.out.println("索引超过数组范围");
            return;
        }
        arr[index] = vaule;
        if (index < useCount){
            return;
        }
        useCount++;
        if (isNeedExtend()) {
            extend();
        }
    }

    /**
     * 在index索引处插入一个值
     * @param index
     * @param value
     */
    public void insert(int index, int value) {
        if (index >= size || index < 0) {
            System.out.println("索引超过数组范围");
            return;
        }
        for (int i = useCount; i > index ; i--) {
            arr[i] = arr[i - 1];
        }
        arr[index] = value;
        useCount++;
        if (useCount > size) {
            System.out.println("数组容量已经达到最大限制，请勿在增加数据");
        }
        if (isNeedExtend()) {
            extend();
        }
    }

    /**
     * 删除index索引处的值
     * @param index
     */
    public void deleteIndex(int index) {
        if (index < useCount) {
            for (int i = index; i < useCount; i++) {
                arr[i] = arr[i + 1];
            }
            useCount--;
        }
        else {
            System.out.println("索引 " + index + " 位置还没有进行赋值");
        }
    }

    /**
     * 判断是否需要扩容
     * @return
     */
    private boolean isNeedExtend() {
        if (size * rate - useCount < 0.000001) {
            return true;
        }
        return false;
    }

    /**
     * 进行扩容
     */
    private void extend() {
        int newSize;
        if (size * 1.5 > Integer.MAX_VALUE) {
            newSize = Integer.MAX_VALUE;
            System.out.println("扩容已经达到极限");
        }
        else {
            newSize = (int) (size * 1.5);
        }
        int[] tmp = new int[newSize];
        System.arraycopy(arr, 0, tmp, 0, useCount);
        arr = tmp;
        size = newSize;
    }

    /**
     * 打印数组
     */
    public void print() {
        System.out.println(Arrays.toString(arr));
    }
}

public class num04 {
    public static void main(String[] args) {
        IntArrayExtend iae = new IntArrayExtend();
        for (int i = 0; i < 1000; i++) {
            iae.setIndex(i,i);
        }
        iae.print();
        iae.setIndex(1,11);
        iae.print();
        iae.insert(0, 22);
        iae.print();
        System.out.println(iae.contains(22));
        System.out.println(iae.getIndex(8));
        iae.deleteIndex(0);
        iae.print();
        System.out.println(iae.getIndex(15));
        iae.deleteIndex(15);

    }
}
