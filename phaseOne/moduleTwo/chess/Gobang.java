package com.asia.homework;


/**
 * created with IntelliJ IDEA.
 * date: 2020/5/2
 * time: 14:49
 *
 * @author: asiaw
 * Description:
 *      一个单机版的五子棋游戏
 */
public class Gobang {

    /**
     * 用于描述五子棋盘的字符数组
     */
    private static String[][] chessboard = new String[17][17];
    /**
     * 棋盘大小
     */
    public static final int SIZE = 17;
    /**
     * 玩家姓名
     */
    private String name;
    /**
     * 标记玩家执白棋还是执黑棋
     */
    private String sign;
    /**
     * 标记玩家是否获得胜利
     */
    private boolean win = false;
    /**
     * 白棋标志
     */
    private static final String WHITECHESS = "●";
    /**
     * 黑棋标志
     */
    private static final String BLACKCHESS = "○";
    /**
     * 棋盘标志
     */
    private static final String CHESSBOARD = "+";

    /**
     * 返回胜利标志
     * @return
     */
    public boolean isWin() {
        return win;
    }

    /**
     * 返回玩家姓名
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * 返回玩家所执棋子
     * @return
     */
    public String getSign() {
        return sign;
    }

    /**
     * 初始化棋盘
     */
    static {
        String[] tr = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f"};
        chessboard[0][0] = " ";
        for (int i = 1; i < SIZE; i++) {
            chessboard[0][i] = tr[i - 1];
        }
        for (int i = 1; i < SIZE; i++) {
            chessboard[i][0] = tr[i - 1];
            for (int j = 1; j < SIZE; j++) {
                chessboard[i][j] = CHESSBOARD;
            }
        }
    }

    /**
     * 无参构造函数，设为私有，因为不想得到没有姓名的参数
     */
    private Gobang() {

    }

    /**
     * 有参构造函数，设置玩家姓名，棋子
     * @param name 玩家名字
     * @param sign 玩家棋子
     */
    public Gobang(String name, String sign) {
        this.name = name;
        this.sign = sign;
    }

    /**
     * 显示棋盘
     */
    public static void showChessBoard() {
        for (int i = 0; i < SIZE; i++) {
            System.out.print("\033[91;1m" + chessboard[0][i] + "\033[0m" + "  ");
        }
        System.out.println();
        for (int i = 1; i < SIZE; i++) {
            System.out.print("\033[91;1m" + chessboard[i][0] + "\033[0m" + "  ");
            for (int j = 1; j < SIZE; j++) {
                if (WHITECHESS.equals(chessboard[i][j])) {
                    System.out.print("\033[30;1m" + chessboard[i][j] + "\033[0m" + "  ");
                }
                else if (BLACKCHESS.equals(chessboard[i][j])) {
                    System.out.print("\033[92;1m" + chessboard[i][j] + "\033[0m" + "  ");
                }
                else {
                    System.out.print("\033[96;1m" + chessboard[i][j] + "\033[0m" + "  ");
                }
            }
            System.out.println();
        }
    }

    /**
     * 玩家落子后更改棋盘状态
     * @param x 落子横坐标
     * @param y 落子纵坐标
     */
    public void setChessboard(int x, int y) {
        chessboard[x][y] = sign;
        // 显示棋盘
        showChessBoard();
        // 检查玩家是否获胜
        if (checkIsWin(x, y)) {
            win = true;
        }
    }

    /**
     * 获取棋盘某个位置的落子情况
     * @param x 横坐标
     * @param y 纵坐标
     * @return
     */
    public static String getChessboard(int x, int y) {
        return chessboard[x][y];
    }

    /**
     * 判断玩家是否获胜
     * @param x 落子横坐标
     * @param y 落子纵坐标
     * @return
     */
    private boolean checkIsWin(int x, int y) {
        return  checkLength(countLength(x,y,0,1)) || checkLength(countLength(x,y,1,0)) ||
                checkLength(countLength(x,y,1,1)) || checkLength(countLength(x,y,1,-1));
    }

    /**
     * 获取连续棋子的长度，a,b为向哪个方向扩展
     * @param x 落子横坐标
     * @param y 落子纵坐标
     * @param a 横坐标扩展方向
     * @param b 纵坐标扩展方向
     * @return
     */
    private int countLength(int x, int y, int a, int b) {
        // 多少个连子
        int length = 0;
        // 一侧扩展
        length += count(x, y, a, b);
        // 另一侧扩展
        length += count(x, y, -1 * a, -1 * b);
        return length - 1;
    }

    /**
     * 获取连续棋子的长度，a,b为向哪个方向扩展
     * @param x 落子横坐标
     * @param y 落子纵坐标
     * @param a 横坐标扩展方向
     * @param b 纵坐标扩展方向
     * @return
     */
    private int count(int x, int y, int a, int b) {
        int length = 0;
        while (x >= 0 && x < SIZE && y >= 0 && y < SIZE && Gobang.getChessboard(x, y) == this.sign) {
            length++;
            x += a;
            y += b;
        }
        return length;
    }

    /**
     * 判断是否连续五子
     * @param length 连续棋子长度
     * @return
     */
    private boolean checkLength(int length) {
        return length > 4 ? true:false;
    }
}
