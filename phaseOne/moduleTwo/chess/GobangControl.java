package com.asia.homework;

import java.util.Scanner;

/**
 * created with IntelliJ IDEA.
 * date: 2020/5/2
 * time: 15:05
 *
 * @author: asiaw
 * Description:
 */
public class GobangControl {

    /**
     * 用于读取输入数据
     */
    private static Scanner in = new Scanner(System.in);

    /**
     * 角色玩家 1
     */
    private Gobang playerOne;
    /**
     * 角色玩家 2
     */
    private Gobang playerTwo;

    /**
     * 白棋标志
     */
    private final String WHITECHESS = "●";
    /**
     * 黑棋标志
     */
    private final String BLACKCHESS = "○";
    /**
     * 棋盘标志
     */
    private final String CHESSBOARD = "+";
    /**
     * 已经落子的棋子数量
     */
    private int countOfChess = 0;
    /**
     * 棋盘容量
     */
    public final int MAXCOUNTOFCHESS = 256;

    /**
     * 最开始的游戏提示函数
     */
    public void show() {
        // 显示棋盘
        Gobang.showChessBoard();
        // 游戏提示
        System.out.println("Hello，看到这丑丑的五子棋盘，想不想和你的小伙伴来一把五子棋，一决胜负？");
        System.out.println("1. 行吧，看在作业的份上玩一把        2. 太垃圾拉，不玩");
        System.out.println("输入 1 开始游戏，其他任意输入退出");

        // 读取是否进行游戏
        String str = in.next();
        // 判断是否要玩
        if (!"1".equals(str)) {
            System.out.println("再见，祝您生活愉快");
            return;
        }
        // 开始游戏
        play();
    }

    /**
     * 游戏进程控制函数
     */
    public void play() {
        // 玩家设置
        set();
        // 游戏开始
        begin();
        // 游戏结束
        end();
    }



    /**
     * 读取玩家信息
     */
    private void set() {
        // 读取玩家输入的名字
        String name;

        // 提示信息
        System.out.println("好的，在游戏开始之前，先给你和你的小伙伴起一个名字吧，然后选择黑色棋子还是白色棋子，黑色棋子先行");
        System.out.println("请输入你的名字（注意不要用空格，程序会出错）：");

        // 读取玩家1名字信息
        name = in.next();

        // 提示信息
        System.out.println("好的，" + name + "接下来选择你的棋子，黑棋先行");
        System.out.println("1. 黑棋标志：" + BLACKCHESS + "  2. 白棋标志： " + WHITECHESS);
        System.out.println("输入 1 选择黑棋，  其他输入选择白棋 ");

        // 读取玩家1选择黑棋还是白棋
        String str = in.next();

        // 为玩家一建立角色档案
        playerOne = new Gobang(name, str.equals("1") ? BLACKCHESS:WHITECHESS);

        // 提示信息
        String ss = "1".equals(str) ? "黑棋" : "白棋";
        System.out.println("好的，" + name + "你选择了" + ss + "，接下来请输入小伙伴的名字");

        // 读取玩家2名字信息
        name = in.next();
        System.out.println(name);

        // 为玩家二建立角色档案
        playerTwo = new Gobang(name, str.equals("1") ? WHITECHESS:BLACKCHESS);

        // 提示信息
        System.out.println("输入完毕，游戏开始");

        // 输出棋盘
        Gobang.showChessBoard();
    }

    /**
     * 游戏正式开始函数
     */
    private void begin() {
        // 置换黑白棋，将玩家一设定为执黑子的玩家
        if (playerOne.getSign() == WHITECHESS) {
            Gobang tmp = playerOne;
            playerOne = playerTwo;
            playerTwo = tmp;
        }
        // 存储落子坐标
        int[] coord;

        // 死循环 玩家下棋
        while (true) {
            // 执黑子玩家落子
            coord = findLocation(playerOne.getName());
            playerOne.setChessboard(coord[0], coord[1]);
            // 检查是否获胜
            if (checkAfterMove(1)) {
                break;
            }
            // 执白子玩家落子
            coord = findLocation(playerTwo.getName());
            playerTwo.setChessboard(coord[0], coord[1]);
            // 检查是否获胜
            if (checkAfterMove(2)) {
                break;
            }
        }
    }

    /**
     * 检查玩家是否获胜或者和棋
     * @param x
     * @return
     */
    private boolean checkAfterMove(int x) {
        if (x == 1) {
            return playerOne.isWin() || isPeace();
        }
        return playerTwo.isWin() || isPeace();
    }

    /**
     * 检查是不是和棋
     * @return
     */
    private boolean isPeace() {
        countOfChess++;
        if (countOfChess == MAXCOUNTOFCHESS) {
            return true;
        }
        return false;
    }

    /**
     * 玩家落子
     * @param name
     * @return
     */
    private int[] findLocation(String name) {
        // 横纵坐标变量
        int x = 0,y = 0;
        try {
            // 坐标输入提示
            System.out.println(name + "，请你输入要落子的位置，用横坐标与纵坐标表示，如棋盘所示");
            System.out.println("横纵坐标范围为0 ~ 9， a ~ f，再提醒输入横纵坐标后输入范围内的单个字符，0~9，a~f，不要输入其他内容");

            // 提示输入横坐标
            System.out.println("请输入横坐标：");

            // 读取横坐标
            String str = in.next();
            // 判断输入是否正确
            if (str.length() > 1) {
                return wrongLocation(name, 0);
            }
            // 转换为数组下标
            x = Integer.parseInt(str.charAt(0) + "",16) + 1;

            // 坐标输入提示
            System.out.println("请输入纵坐标：");

            // 读取纵坐标
            str = in.next();
            // 判断输入是否正确
            if (str.length() > 1) {
                return wrongLocation(name, 0);
            }
            // 转换为数组下标
            y = Integer.parseInt(str.charAt(0) + "",16) + 1;

            // 简单判断输入是否有误，如有重新输入
            if (x < 0 || x > Gobang.SIZE || y < 0 || y > Gobang.SIZE) {
                return wrongLocation(name, 0);
            }
            else {
                // 判断坐标是否已经落子，如有重新输入
                if (Gobang.getChessboard(x, y) != CHESSBOARD) {
                    return wrongLocation(name, 1);
                }
            }
        }
        catch (Exception e) {
            return wrongLocation(name, 0);
        }

        // 返回落子坐标
        return new int[]{x, y};
    }

    /**
     * 输入有误后提示
     * @return
     */
    private int[] wrongLocation(String name, int x) {
        if (x == 0) {
            System.out.println("坐标输入有误，请重新输入坐标");
        }
        else {
            System.out.println("该位置已经有棋子落下，请重新输入坐标");
        }
        return findLocation(name);
    }

    /**
     * 游戏结束：和棋或者玩家之一获得胜利
     */
    private void end() {
        // 和棋
        if (countOfChess == MAXCOUNTOFCHESS) {
            System.out.println("哇，" + playerOne.getName() + "，你和" + playerTwo.getName() + "棋逢对手");
        }
        // 玩家1获胜
        else if (playerOne.isWin()) {
            draw(playerOne.getName());
        }
        // 玩家2获胜
        else {
            draw(playerTwo.getName());
        }
        drawChecken();
    }

    /**
     * 输出小鸡图案
     */
    private void drawChecken() {
        System.out.println("      .\".\".\".");
        System.out.println("    (`       `)               _.-=-.");
        System.out.println("     '._.--.-;             .-`  -'  '.");
        System.out.println("    .-'`.o )  \\           /  .-_.--'  `\\");
        System.out.println("   `;---) \\    ;         /  / ;' _-_.-' `");
        System.out.println("     `;\"`  ;    \\        ; .  .'   _-' \\");
        System.out.println("      (    )    |        |  / .-.-'    -`");
        System.out.println("       '-.-'     \\       | .' ` '.-'-\\`");
        System.out.println("        /_./\\_.|\\_\\      ;  ' .'-'.-.");
        System.out.println("        /         '-._    \\` /  _;-,");
        System.out.println("       |         .-=-.;-._ \\  -'-,");
        System.out.println("       \\        /      `\";`-`,-\"`)");
        System.out.println("        \\       \\     '-- `\\.\\");
        System.out.println("         '.      '._ '-- '--'/");
        System.out.println("           `-._     `'----'`;");
        System.out.println("               `\"\"\"--.____,/");
        System.out.println("                      \\\\  \\");
        System.out.println("                      // /`");
        System.out.println("                  ___// /__");
        System.out.println("                (`(`(---\"-`)");
    }

    /**
     * 输出获胜信息
     * @param name
     */
    private void draw(String name) {
        System.out.println(name + "，恭喜你获得胜利，大吉大利，今晚吃鸡");
    }

//    public static void main(String[] args) {
//
//        // 建立游戏控制变量
//        GobangControl gc = new GobangControl();
//        // 输出游戏详情
//        gc.show();
//    }
}
