package com.asia.homework;

/**
 * created with IntelliJ IDEA.
 * date: 2020/5/3
 * time: 19:00
 *
 * @author: asiaw
 * Description:
 *      通话套餐类
 */
public class PhonePackages extends AbstractPackages implements PhoneServices{

    /**
     * 通话时长，设为int，因为现在好像都是按分钟计费，不满一分钟也按一分钟算
     */
    private int lengthOfCall;
    /**
     * 短信条数
     */
    private int countOfMessage;
//    /**
//     * 每月资费，好像没有小数的费用，设为整数
//     */
//    继承父类的属性
//    private int chargesMonth;

    public PhonePackages() {
    }

    /**
     * 有参构造函数
     * @param lengthOfCall
     * @param countOfMessage
     * @param chargesMonth
     */
    public PhonePackages(int lengthOfCall, int countOfMessage, int chargesMonth) {
        super(chargesMonth);
        this.lengthOfCall = lengthOfCall;
        this.countOfMessage = countOfMessage;
    }

    public int getLengthOfCall() {
        return lengthOfCall;
    }

    public void setLengthOfCall(int lengthOfCall) {
        this.lengthOfCall = lengthOfCall;
    }

    public int getCountOfMessage() {
        return countOfMessage;
    }

    public void setCountOfMessage(int countOfMessage) {
        this.countOfMessage = countOfMessage;
    }

    @Override
    public void show() {
        System.out.println(
            "套餐信息：\n" +
            "   通话时长（分钟）：" + lengthOfCall + "\n" +
            "   短信条数（条）：" + countOfMessage + "\n" +
            "   每月资费（元）：" + getChargesMonth()
        );
    }

    @Override
    public String toString() {
        return "PhonePackages{" +
                "lengthOfCall=" + lengthOfCall +
                ", countOfMessage=" + countOfMessage +
                ", chargesMonth=" + getChargesMonth() +
                '}';
    }

    @Override
    public void phone(int lengthOfCall, Sims sim) {
        System.out.println("手机卡类型：" + sim.getType().getType());
        System.out.println("通话时间（分钟）：" + lengthOfCall);
    }
}
