package com.asia.homework;

/**
 * created with IntelliJ IDEA.
 * date: 2020/5/3
 * time: 19:18
 *
 * @author: asiaw
 * Description:
 *      用户消费信息类
 */
public class UserConsumeInfo {

    /**
     * 总通话时长，整数类型，单位分钟
     */
    private int totalTalkTime;
    /**
     * 总上网流量，用double类型，单位 G
     */
    private double totalTraffic;
    /**
     * 每月消费金额，好像会出现小数，使用double类型
     */
    private double cost;

    public UserConsumeInfo() {
    }

    public UserConsumeInfo(int totalTalkTime, double totalTraffic, double cost) {
        this.totalTalkTime = totalTalkTime;
        this.totalTraffic = totalTraffic;
        this.cost = cost;
    }

    public int getTotalTalkTime() {
        return totalTalkTime;
    }

    public void setTotalTalkTime(int totalTalkTime) {
        this.totalTalkTime = totalTalkTime;
    }

    public double getTotalTraffic() {
        return totalTraffic;
    }

    public void setTotalTraffic(double totalTraffic) {
        this.totalTraffic = totalTraffic;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }
}
