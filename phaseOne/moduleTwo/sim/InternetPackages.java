package com.asia.homework;

/**
 * created with IntelliJ IDEA.
 * date: 2020/5/3
 * time: 19:12
 *
 * @author: asiaw
 * Description:
 *      上网套餐类
 */
public class InternetPackages extends AbstractPackages implements InternetService{

    /**
     * 上网流量，单位G
     */
    private double traffic;

    public InternetPackages() {
    }

    public InternetPackages(double traffic, int chargesMonth) {
        super(chargesMonth);
        this.traffic = traffic;
    }

    public double getTraffic() {
        return traffic;
    }

    public void setTraffic(double traffic) {
        this.traffic = traffic;
    }

    /**
     * 重写抽象父类的行为显示方法
     */
    @Override
    public void show() {
        System.out.println("上网套餐信息：\n" +
                "上网流量（G）：" + traffic + "\n" +
                "每月资费（元）：" + getChargesMonth()
                );
    }

    /**
     * 重写上网服务接口的上网抽象方法
     * @param traffic   上网流量
     * @param sim       手机卡类型
     */
    @Override
    public void surfing(double traffic, Sims sim) {
        System.out.println("手机卡类型：" + sim.getType().getType());
        System.out.println("上网流量为：" + traffic);
    }
}
