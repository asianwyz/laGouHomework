package com.asia.homework;

/**
 * created with IntelliJ IDEA.
 * date: 2020/5/3
 * time: 18:47
 *
 * @author: asiaw
 * Description:
 *      手机卡类
 */
public class Sims {

    /**
     * 手机卡类型
     */
    private TypeOfSim type;
    /**
     * 卡号
     */
    private long number;
    /**
     * 用户名
     */
    private String username;
    /**
     * 密码
     */
    private String password;
    /**
     * 账户余额
     */
    private double balance;
    /**
     * 通话时长
     */
    private int lengthOfCall;
    /**
     * 上网流量
     */
    private double traffic;

    public Sims() {
    }

    public Sims(TypeOfSim type, long number, String username, String password, double balance,
                int lengthOfCall, double traffic) {
        this.type = type;
        this.number = number;
        this.username = username;
        this.password = password;
        this.balance = balance;
        this.lengthOfCall = lengthOfCall;
        this.traffic = traffic;
    }

    public TypeOfSim getType() {
        return type;
    }

    public void setType(TypeOfSim type) {
        this.type = type;
    }

    public long getNumber() {
        return number;
    }

    public void setNumber(long number) {
        this.number = number;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public int getLengthOfCall() {
        return lengthOfCall;
    }

    public void setLengthOfCall(int lengthOfCall) {
        this.lengthOfCall = lengthOfCall;
    }

    public double getTraffic() {
        return traffic;
    }

    public void setTraffic(double traffic) {
        this.traffic = traffic;
    }

    public void show() {
        System.out.println(number + "号码用户的名字为 " + username + "，账户余额 " + balance + "元");
    }
}
