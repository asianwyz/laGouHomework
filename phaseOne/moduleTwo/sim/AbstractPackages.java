package com.asia.homework;

/**
 * created with IntelliJ IDEA.
 * date: 2020/5/4
 * time: 10:22
 *
 * @author: asiaw
 * Description:
 */
public abstract class AbstractPackages {

    /**
     * 每月资费，设为整数，好像没看见套餐费带小数点
     */
    private int chargesMonth;

    public AbstractPackages() {
    }

    public AbstractPackages(int chargesMonth) {
        this.chargesMonth = chargesMonth;
    }

    /**
     * 显示所有套餐信息
     */
    public abstract void show();

    public int getChargesMonth() {
        return chargesMonth;
    }

    public void setChargesMonth(int chargesMonth) {
        this.chargesMonth = chargesMonth;
    }

}
