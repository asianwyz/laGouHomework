package com.asia.homework;

/**
 * created with IntelliJ IDEA.
 * date: 2020/5/4
 * time: 10:33
 *
 * @author: asiaw
 * Description:
 *      通话服务接口
 */
public interface PhoneServices {

    /**
     * 通话服务方法，参数：1. 通话分钟 2. 手机卡类对象
     * @param lengthOfCall      通话时长
     * @param sim               手机卡类型
     */
    void phone(int lengthOfCall, Sims sim);
}
