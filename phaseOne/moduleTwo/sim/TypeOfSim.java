package com.asia.homework;

/**
 * created with IntelliJ IDEA.
 * date: 2020/5/3
 * time: 19:24
 *
 * @author: asiaw
 * Description:
 */
public enum TypeOfSim {

    /**
     * 大卡类型
     */
    BIG("大卡"),
    /**
     * 小卡类型
     */
    SMALL("小卡"),
    /**
     * 微型卡类型
     */
    MICRO("微型卡");


    public String getType() {
        return type;
    }

    /**
     * 手机卡类型
     */
    private String type;


    private TypeOfSim(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "TypeOfSim{" +
                "type='" + type + '\'' +
                '}';
    }
}
