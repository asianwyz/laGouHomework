package com.asia.homework;

/**
 * created with IntelliJ IDEA.
 * date: 2020/5/4
 * time: 10:35
 *
 * @author: asiaw
 * Description:
 *      上网服务接口
 */
public interface InternetService {

    /**
     * 提供上网服务
     * @param traffic   上网流量
     * @param sim       手机卡类型
     */
    void surfing(double traffic, Sims sim);
}
